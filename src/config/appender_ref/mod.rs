//! AppenderRef
use config::filter::FilterType;
use std::default::Default;

/// AppenderRef struct
pub struct AppenderRef {
    /// AppenderRef name.  This should match an appender name.
    pub name: String,
    /// Filters associated with the AppenderRef.
    pub filters: Vec<Box<::Filter>>,
}

impl<'a> From<&'a AppenderRefType> for AppenderRef {
    fn from(t: &'a AppenderRefType) -> AppenderRef {
        AppenderRef {
            name: t.name.clone(),
            filters: match t.filter.clone() {
                Some(f) => f.into(),
                None => Vec::new(),
            },
        }
    }
}

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// AppenderRefType struct
pub struct AppenderRefType {
    /// AppenderRefType name.
    name: String,
    /// Filters associated with the AppenderRefType.
    filter: Option<FilterType>,
}

impl Default for AppenderRefType {
    fn default() -> AppenderRefType {
        AppenderRefType {
            name: "console".to_owned(),
            filter: None,
        }
    }
}

impl AppenderRefType {
    /// Create a new AppenderRefType with the given name.
    pub fn new(name: String) -> AppenderRefType {
        AppenderRefType { name: name, ..Default::default() }
    }

    /// Add optional filter(s) to the AppenderRefType.
    pub fn filter(mut self, filter: Option<FilterType>) -> AppenderRefType {
        self.filter = filter;
        self
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use rustc_serialize::{Decodable, Decoder};
    use super::*;

    impl Decodable for AppenderRefType {
        fn decode<D: Decoder>(d: &mut D) -> Result<AppenderRefType, D::Error> {
            d.read_struct("AppenderRefType", 2, |d| {
                let name = try!(d.read_struct_field("name", 1, |d| Decodable::decode(d)));
                let filter_type = try!(d.read_struct_field("filter", 2, |d| Decodable::decode(d)));
                Ok(AppenderRefType::new(name).filter(filter_type))
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};
    use super::*;

    enum AppenderRefTypeField {
        Name,
        Filter,
    }

    impl Deserialize for AppenderRefTypeField {
        fn deserialize<D>(deserializer: &mut D) -> Result<AppenderRefTypeField, D::Error>
            where D: Deserializer
        {
            struct AppenderRefTypeFieldVisitor;

            impl Visitor for AppenderRefTypeFieldVisitor {
                type Value = AppenderRefTypeField;

                fn visit_str<E>(&mut self, value: &str) -> Result<AppenderRefTypeField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(AppenderRefTypeField::Name),
                        "filter" => Ok(AppenderRefTypeField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(AppenderRefTypeFieldVisitor)
        }
    }

    impl Deserialize for AppenderRefType {
        fn deserialize<D>(deserializer: &mut D) -> Result<AppenderRefType, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "filter"];
            deserializer.visit_struct("AppenderRefType", FIELDS, AppenderRefTypeVisitor)
        }
    }

    struct AppenderRefTypeVisitor;

    impl Visitor for AppenderRefTypeVisitor {
        type Value = AppenderRefType;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<AppenderRefType, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(AppenderRefTypeField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderRefTypeField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let n = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            try!(visitor.end());
            Ok(AppenderRefType::new(n).filter(filter))
        }
    }
}

#[cfg(test)]
mod test {
    use {Named, decode};
    use super::*;

    const BASE_CONFIG: &'static str = r#"
name = "noop"
    "#;
    const ALL_CONFIG: &'static str = r#"
name = "noop"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
    blah = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
name = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2];

    #[test]
    fn test_default() {
        let art: AppenderRefType = Default::default();
        assert!(art.name == "console");
        assert!(art.filter.is_none());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<AppenderRefType>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => {
                    assert!(false);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<AppenderRefType>(invalid) {
                Ok(_) => {
                    assert!(false);
                }
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
