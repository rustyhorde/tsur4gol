//! tsur4gol Configuration
use Filter;
use config::appender::{Appender, AppenderType};
use config::filter::FilterType;
use config::logger::Logger;
use self::ConfigError::*;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::{self, Read};
use std::path::Path;
use time::Duration;
use toml;

pub mod appender;
pub mod appender_ref;
pub mod filter;
pub mod logger;

/// Read a TOML tsur4gol config file.
pub fn read_config(path: &Path) -> Result<String, io::Error> {
    let mut file = try!(File::open(path));
    let mut s = String::new();
    try!(file.read_to_string(&mut s));
    Ok(s)
}

#[derive(Debug, Default)]
/// tsur4gol Configuration
pub struct Config {
    /// Configuration change check duration.
    refresh_rate: Option<Duration>,
    /// Configured appenders.
    appender: Option<AppenderType>,
    /// Configured loggers.
    logger: Option<Vec<Logger>>,
    /// Configured config-level filters.
    filter: Option<FilterType>,
}

impl Config {
    #[cfg(feature = "rustc-serialize")]
    /// Parse a TOML string into a tsur4gol Config.
    pub fn new(tomlstr: &str) -> Result<Config, ConfigError> {
        use rustc_serialize::Decodable;

        let mut parser = toml::Parser::new(tomlstr);
        match parser.parse() {
            Some(toml) => {
                let mut decoder = toml::Decoder::new(toml::Value::Table(toml));
                match Config::decode(&mut decoder) {
                    Ok(c) => Ok(c),
                    Err(e) => Err(DecodeError(e)),
                }
            }
            None => Err(ParserError(parser.errors)),
        }
    }

    #[cfg(all(not(feature = "rustc-serialize"), feature = "serde"))]
    /// Parse a TOML string into a tsur4gol Config.
    pub fn new(tomlstr: &str) -> Result<Config, ConfigError> {
        use serde::Deserialize;

        let mut parser = toml::Parser::new(tomlstr);
        match parser.parse() {
            Some(toml) => {
                let mut decoder = toml::Decoder::new(toml::Value::Table(toml));
                match Deserialize::deserialize(&mut decoder) {
                    Ok(c) => Ok(c),
                    Err(e) => Err(DecodeError(e)),
                }
            }
            None => Err(ParserError(parser.errors)),
        }
    }

    /// Create a new default Config.
    pub fn builder() -> Config {
        Default::default()
    }

    /// Set the refresh_rate for this config.
    pub fn refresh_rate(mut self, refresh_rate: Option<Duration>) -> Config {
        self.refresh_rate = refresh_rate;
        self
    }

    /// Set the appender for this config.
    pub fn appender(mut self, appender: Option<AppenderType>) -> Config {
        self.appender = appender;
        self
    }

    /// Set the filter for this config.
    pub fn filter(mut self, filter: Option<FilterType>) -> Config {
        self.filter = filter;
        self
    }

    /// Set the logger for this config.
    pub fn logger(mut self, logger: Option<Vec<Logger>>) -> Config {
        self.logger = logger;
        self
    }

    /// Unpack the configuration.
    pub fn unpack(self) -> (Vec<Logger>, Vec<Logger>, Vec<Appender>, Vec<Box<Filter>>) {
        let mut loggers = Vec::new();

        let cloggers = match self.logger {
            Some(l) => l,
            None => vec![],
        };

        let mut root = Vec::new();

        for logger in cloggers {
            if logger.name() == "root" {
                root.push(logger)
            } else {
                loggers.push(logger);
            }
        }

        if root.is_empty() {
            root.push(Default::default());
        }

        let appenders = match self.appender {
            Some(a) => a.into(),
            None => {
                let default: AppenderType = Default::default();
                default.into()
            }
        };

        let filters = match self.filter {
            Some(f) => f.into(),
            None => Vec::new(),
        };

        (root, loggers, appenders, filters)
    }

    /// Get the refresh_rate for this config.
    pub fn get_refresh_rate(&self) -> Option<Duration> {
        self.refresh_rate
    }
}

#[cfg(feature = "rustc-serialize")]
pub mod rs {
    //! rustc_serialize decodable implementations for LogLevelFilter and NaiveTime.
    use super::*;
    use chrono::NaiveTime;
    use log::LogLevelFilter;
    use rustc_serialize::{Decodable, Decoder};
    use std::str::FromStr;
    use time::Duration;

    /// Read a LogLevelFilter.
    pub fn read_llf<D: Decoder>(d: &mut D) -> Result<LogLevelFilter, D::Error> {
        let field = try!(d.read_str());
        match LogLevelFilter::from_str(&field[..]) {
            Ok(llf) => Ok(llf),
            Err(e) => {
                Err(d.error(&*format!("Could not convert '{}' to a LogLevelFilter! {:?}",
                                      field,
                                      e)))
            }
        }
    }

    /// Read an optional LogLevelFilter.
    pub fn read_llf_opt<D: Decoder>(d: &mut D,
                                    r: bool)
                                    -> Result<Option<LogLevelFilter>, D::Error> {
        if r {
            match read_llf(d) {
                Ok(llf) => Ok(Some(llf)),
                Err(e) => Err(e),
            }
        } else {
            Ok(Some(LogLevelFilter::Warn))
        }
    }

    /// Read a NaiveTime.
    pub fn read_naivetime<D: Decoder>(d: &mut D) -> Result<NaiveTime, D::Error> {
        let start = try!(d.read_str());
        match NaiveTime::parse_from_str(&start[..], "%H:%M:%S") {
            Ok(s) => Ok(s),
            Err(e) => Err(d.error(&*format!("Error parsing start time! {:?}", e))),
        }
    }

    /// Read an optional NaiveTime.
    pub fn read_naivetime_opt<D: Decoder>(d: &mut D,
                                          r: bool)
                                          -> Result<Option<NaiveTime>, D::Error> {
        if r {
            match read_naivetime(d) {
                Ok(nt) => Ok(Some(nt)),
                Err(e) => Err(e),
            }
        } else {
            Ok(None)
        }
    }

    impl Decodable for Config {
        fn decode<D: Decoder>(d: &mut D) -> Result<Config, D::Error> {
            d.read_struct("Config", 4, |d| {
                Ok(Config {
                    refresh_rate: {
                        try!(d.read_struct_field("refresh_rate", 1, |d| {
                            d.read_option(|d, r| {
                                if r {
                                    Ok(Some(Duration::seconds(try!(d.read_i64()))))
                                } else {
                                    Ok(None)
                                }
                            })
                        }))
                    },
                    appender: try!(d.read_struct_field("appender", 2, |d| Decodable::decode(d))),
                    logger: try!(d.read_struct_field("logger", 3, |d| Decodable::decode(d))),
                    filter: try!(d.read_struct_field("filter", 4, |d| Decodable::decode(d))),
                })
            })
        }
    }
}

#[cfg(feature = "serde")]
/// ! Wrapper structs and serde implementations for LogLevelFilter and NaiveTime.
pub mod serde {
    use chrono::NaiveTime;
    use config::appender::AppenderType;
    use config::filter::FilterType;
    use config::logger::Logger;
    use log::LogLevelFilter;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};
    use super::*;
    use std::error::Error;
    use time::Duration;

    /// LogLevelFilter Wrapper for serde.
    pub struct LogLevelFilterField {
        level: LogLevelFilter,
    }

    impl Default for LogLevelFilterField {
        fn default() -> LogLevelFilterField {
            LogLevelFilterField { level: LogLevelFilter::Warn }
        }
    }

    impl LogLevelFilterField {
        /// Get the wrapped LogLevelFilter value.
        pub fn level(&self) -> LogLevelFilter {
            self.level
        }
    }

    impl Deserialize for LogLevelFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<LogLevelFilterField, D::Error>
            where D: Deserializer
        {
            struct LogFilterFieldVisitor;

            impl Visitor for LogFilterFieldVisitor {
                type Value = LogLevelFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<LogLevelFilterField, E>
                    where E: ::serde::de::Error
                {
                    let mut llff: LogLevelFilterField = Default::default();

                    match value {
                        "Off" => llff.level = LogLevelFilter::Off,
                        "Error" => llff.level = LogLevelFilter::Error,
                        "Warn" => llff.level = LogLevelFilter::Warn,
                        "Info" => llff.level = LogLevelFilter::Info,
                        "Debug" => llff.level = LogLevelFilter::Debug,
                        "Trace" => llff.level = LogLevelFilter::Trace,
                        _ => {
                            return Err(::serde::de::Error::syntax("Unexpected LogLevelFilter \
                                                                   value!"))
                        }
                    }

                    Ok(llff)
                }
            }

            deserializer.visit(LogFilterFieldVisitor)
        }
    }

    /// NaiveTime wrapper for serde.
    pub struct NaiveTimeField {
        naive_time: NaiveTime,
    }

    impl Default for NaiveTimeField {
        fn default() -> NaiveTimeField {
            NaiveTimeField { naive_time: NaiveTime::from_hms(0, 0, 0) }
        }
    }

    impl NaiveTimeField {
        /// Get the NaiveTime value from the wrapper.
        pub fn naive_time(&self) -> NaiveTime {
            self.naive_time
        }
    }

    impl Deserialize for NaiveTimeField {
        fn deserialize<D>(deserializer: &mut D) -> Result<NaiveTimeField, D::Error>
            where D: Deserializer
        {
            struct NaiveTimeFieldVisitor;

            impl Visitor for NaiveTimeFieldVisitor {
                type Value = NaiveTimeField;

                fn visit_str<E>(&mut self, value: &str) -> Result<NaiveTimeField, E>
                    where E: ::serde::de::Error
                {
                    let mut nt: NaiveTimeField = Default::default();

                    match NaiveTime::parse_from_str(value, "%H:%M:%S") {
                        Ok(t) => nt.naive_time = t,
                        Err(e) => return Err(::serde::de::Error::syntax(e.description())),
                    }

                    Ok(nt)
                }
            }

            deserializer.visit(NaiveTimeFieldVisitor)
        }
    }

    /// Duration wrapper for serde.
    pub struct DurationField {
        duration: Duration,
    }

    impl Default for DurationField {
        fn default() -> DurationField {
            DurationField { duration: Duration::seconds(0) }
        }
    }

    impl DurationField {
        /// Get the Duration value from the wrapper.
        pub fn duration(&self) -> Duration {
            self.duration
        }
    }

    impl Deserialize for DurationField {
        fn deserialize<D>(deserializer: &mut D) -> Result<DurationField, D::Error>
            where D: Deserializer
        {
            struct DurationFieldVisitor;

            impl Visitor for DurationFieldVisitor {
                type Value = DurationField;

                fn visit_i64<E>(&mut self, value: i64) -> Result<DurationField, E>
                    where E: ::serde::de::Error
                {
                    let mut dur: DurationField = Default::default();
                    dur.duration = Duration::seconds(value);

                    Ok(dur)
                }
            }

            deserializer.visit(DurationFieldVisitor)
        }
    }

    enum ConfigField {
        RefreshRate,
        Appender,
        Logger,
        Filter,
    }

    impl Deserialize for ConfigField {
        fn deserialize<D>(deserializer: &mut D) -> Result<ConfigField, D::Error>
            where D: Deserializer
        {
            struct ConfigFieldVisitor;

            impl Visitor for ConfigFieldVisitor {
                type Value = ConfigField;

                fn visit_str<E>(&mut self, value: &str) -> Result<ConfigField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "refresh_rate" => Ok(ConfigField::RefreshRate),
                        "appender" => Ok(ConfigField::Appender),
                        "logger" => Ok(ConfigField::Logger),
                        "filter" => Ok(ConfigField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(ConfigFieldVisitor)
        }
    }

    impl Deserialize for Config {
        fn deserialize<D>(deserializer: &mut D) -> Result<Config, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["refresh_rate",
                                                       "appender",
                                                       "logger",
                                                       "filter"];
            deserializer.visit_struct("Config", FIELDS, ConfigVisitor)
        }
    }

    struct ConfigVisitor;

    impl Visitor for ConfigVisitor {
        type Value = Config;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<Config, V::Error>
            where V: MapVisitor
        {
            let mut refresh_rate: Option<DurationField> = None;
            let mut appender: Option<AppenderType> = None;
            let mut logger: Option<Vec<Logger>> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(ConfigField::RefreshRate) => {
                        refresh_rate = Some(try!(visitor.visit_value()));
                    }
                    Some(ConfigField::Appender) => {
                        appender = Some(try!(visitor.visit_value()));
                    }
                    Some(ConfigField::Logger) => {
                        logger = Some(try!(visitor.visit_value()));
                    }
                    Some(ConfigField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }


            let rr = match refresh_rate {
                Some(dur) => Some(dur.duration()),
                None => None,
            };

            try!(visitor.end());

            Ok(Config::builder()
                   .refresh_rate(rr)
                   .appender(appender)
                   .logger(logger)
                   .filter(filter))
        }
    }
}

#[derive(Debug)]
/// Invalid Configuration Errors.
pub enum ConfigError {
    /// Parser Errors.
    ParserError(Vec<toml::ParserError>),
    /// Decode Error.
    DecodeError(toml::DecodeError),
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParserError(ref v) => {
                for e in v {
                    try!(writeln!(f, "{}", e));
                }
                Ok(())
            }
            DecodeError(ref e) => writeln!(f, "{}", e),
        }
    }
}

impl Error for ConfigError {
    fn description(&self) -> &str {
        match *self {
            ParserError(_) => "ParserError",
            DecodeError(_) => "DecodeError",
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            ParserError(ref v) => Some(&v[0]),
            DecodeError(ref d) => Some(d),
        }
    }
}

#[cfg(test)]
impl From<toml::DecodeError> for ConfigError {
    fn from(e: toml::DecodeError) -> ConfigError {
        ConfigError::DecodeError(e)
    }
}

#[cfg(test)]
pub mod test {
    use super::*;
    use time::Duration;
    use toml;

    pub const TEST_CONFIG: &'static str = r#"
refresh_rate = 30

[filter.threshold]
level = "Trace"

[[appender.stdout]]
name = "console"

  [appender.stdout.filter.burst]
  max_burst = 5.0

  [appender.stdout.filter.threshold]
  level = "Warn"
  on_match = "Neutral"
  on_mismatch = "deny"

[[appender.stdout]]
name = "console_two"

[[appender.stderr]]
name = "console_err"

[[appender.file]]
name = "logfile"
path = "some/path/from/here"

[[logger]]
name = "config::filter::burst"
level = "Trace"
additive = false

  [[logger.appender_ref]]
  name = "console_two"

    [logger.appender_ref.filter.burst]
    max_burst = 10.0
    on_mismatch = "Deny"

  [logger.filter.threshold]
  level = "Debug"

[[logger]]
name = "config::appender::file"
level = "Trace"
additive = false
"#;

    pub const ONLY_ROOT: &'static str = r#"
[[logger]]
name = "root"
"#;

    pub const BAD_REFRESH_RATE: &'static str = r#"
refresh_rate = "alpha"
    "#;

    pub const PARSER_ERROR: &'static str = r#"
[a]
b = 1
[a]
c = 2
    "#;

    pub const UNEXPECTED_FIELD: &'static str = r#"
[a]
b = 1
    "#;

    #[test]
    pub fn test_valid_everything() {
        match Config::new(TEST_CONFIG) {
            Ok(c) => {
                assert!(c.refresh_rate == Some(Duration::seconds(30)));
                assert!(c.filter.is_some());
            }
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_only_root() {
        match Config::new(ONLY_ROOT) {
            Ok(c) => {
                let (root, _, _, _) = c.unpack();
                assert!(!root.is_empty() && root.len() == 1);
            }
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_unexpected_field() {
        assert!(Config::new(UNEXPECTED_FIELD).is_err());
    }

    #[test]
    fn test_invalid_refresh_rate() {
        assert!(Config::new(BAD_REFRESH_RATE).is_err());
    }

    #[test]
    fn test_parser_error() {
        assert!(Config::new(PARSER_ERROR).is_err());
    }

    #[test]
    fn test_config_error() {
        let mut tomlerr = Vec::new();
        tomlerr.push(toml::ParserError {
            lo: 0,
            hi: 1,
            desc: "Test Error".to_owned(),
        });

        let config_err = ConfigError::ParserError(tomlerr);
        println!("{} {:?}", config_err, config_err);

        let decode_err = ConfigError::DecodeError(toml::DecodeError {
            field: None,
            kind: toml::DecodeErrorKind::SyntaxError,
        });
        println!("{} {:?}", decode_err, decode_err);
    }
}
