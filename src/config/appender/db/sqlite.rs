//! Sqlite Appender
use {Append, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::error::Error;

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug, Default)]
/// Sqlite Appender
pub struct SqliteAppender {
    /// Appender name
    name: String,
    /// Database name
    database: String,
    /// Filters associated with the appender.
    filter: Option<FilterType>,
}

impl SqliteAppender {
    /// Create a new default SqliteAppender.
    pub fn new() -> SqliteAppender {
        Default::default()
    }

    /// Set the name of the SqliteAppender.
    pub fn name(mut self, name: String) -> SqliteAppender {
        self.name = name;
        self
    }

    /// Set the database name for the SqliteAppender.
    pub fn database(mut self, database: String) -> SqliteAppender {
        self.database = database;
        self
    }

    /// Set the optional filters for the SqliteAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> SqliteAppender {
        self.filter = filter;
        self
    }
}

impl Named for SqliteAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for SqliteAppender {
    fn append(&mut self, _record: &LogRecord) -> Result<(), Box<Error>> {
        Ok(())
    }
}

impl Into<Appender> for SqliteAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum SqliteAppenderField {
        Name,
        Database,
        Filter,
    }

    impl Deserialize for SqliteAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<SqliteAppenderField, D::Error>
            where D: Deserializer
        {
            struct SqliteAppenderFieldVisitor;

            impl Visitor for SqliteAppenderFieldVisitor {
                type Value = SqliteAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<SqliteAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(SqliteAppenderField::Name),
                        "database" => Ok(SqliteAppenderField::Database),
                        "filter" => Ok(SqliteAppenderField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(SqliteAppenderFieldVisitor)
        }
    }

    impl Deserialize for SqliteAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<SqliteAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "database", "filter"];
            deserializer.visit_struct("SqliteAppender", FIELDS, SqliteAppenderVisitor)
        }
    }

    struct SqliteAppenderVisitor;

    impl Visitor for SqliteAppenderVisitor {
        type Value = SqliteAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<SqliteAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut database: Option<String> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(SqliteAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(SqliteAppenderField::Database) => {
                        database = Some(try!(visitor.visit_value()));
                    }
                    Some(SqliteAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let db = match database {
                Some(d) => d,
                None => return visitor.missing_field("database"),
            };

            try!(visitor.end());

            Ok(SqliteAppender::new().name(nm).database(db).filter(filter))
        }
    }
}
