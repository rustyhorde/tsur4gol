//! MySQL Appender
use {Append, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::error::Error;

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug, Default)]
/// MySQL Appender
pub struct MysqlAppender {
    /// Appender name.
    name: String,
    /// Database name.
    database: String,
    /// Filters associated with the appender.
    filter: Option<FilterType>,
}

impl MysqlAppender {
    /// Create a new default MysqlAppender.
    pub fn new() -> MysqlAppender {
        Default::default()
    }

    /// Set the name of the MysqlAppender.
    pub fn name(mut self, name: String) -> MysqlAppender {
        self.name = name;
        self
    }

    /// Set the database name for the MysqlAppender.
    pub fn database(mut self, database: String) -> MysqlAppender {
        self.database = database;
        self
    }

    /// Set the optional filters for the MysqlAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> MysqlAppender {
        self.filter = filter;
        self
    }
}

impl Named for MysqlAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for MysqlAppender {
    fn append(&mut self, _record: &LogRecord) -> Result<(), Box<Error>> {
        Ok(())
    }
}

impl Into<Appender> for MysqlAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum MysqlAppenderField {
        Name,
        Database,
        Filter,
    }

    impl Deserialize for MysqlAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<MysqlAppenderField, D::Error>
            where D: Deserializer
        {
            struct MysqlAppenderFieldVisitor;

            impl Visitor for MysqlAppenderFieldVisitor {
                type Value = MysqlAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<MysqlAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(MysqlAppenderField::Name),
                        "database" => Ok(MysqlAppenderField::Database),
                        "filter" => Ok(MysqlAppenderField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(MysqlAppenderFieldVisitor)
        }
    }

    impl Deserialize for MysqlAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<MysqlAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "database", "filter"];
            deserializer.visit_struct("MysqlAppender", FIELDS, MysqlAppenderVisitor)
        }
    }

    struct MysqlAppenderVisitor;

    impl Visitor for MysqlAppenderVisitor {
        type Value = MysqlAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<MysqlAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut database: Option<String> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(MysqlAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(MysqlAppenderField::Database) => {
                        database = Some(try!(visitor.visit_value()));
                    }
                    Some(MysqlAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let db = match database {
                Some(d) => d,
                None => return visitor.missing_field("database"),
            };

            try!(visitor.end());

            Ok(MysqlAppender::new().name(nm).database(db).filter(filter))
        }
    }
}
