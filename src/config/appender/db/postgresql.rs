//! PostgreSQL Appender
use {Append, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::error::Error;

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug, Default)]
/// PostgreSQL Appender
pub struct PostgresqlAppender {
    /// Appender name.
    name: String,
    /// Database name.
    database: String,
    /// Filters associated with appender.
    filter: Option<FilterType>,
}

impl PostgresqlAppender {
    /// Create a new default PostgresqlAppender.
    pub fn new() -> PostgresqlAppender {
        Default::default()
    }

    /// Set the name of the PostgresqlAppender.
    pub fn name(mut self, name: String) -> PostgresqlAppender {
        self.name = name;
        self
    }

    /// Set the database name for the PostgresqlAppender.
    pub fn database(mut self, database: String) -> PostgresqlAppender {
        self.database = database;
        self
    }

    /// Set the optional filters for the PostgresqlAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> PostgresqlAppender {
        self.filter = filter;
        self
    }
}

impl Named for PostgresqlAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for PostgresqlAppender {
    fn append(&mut self, _record: &LogRecord) -> Result<(), Box<Error>> {
        Ok(())
    }
}

impl Into<Appender> for PostgresqlAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum PostgresqlAppenderField {
        Name,
        Database,
        Filter,
    }

    impl Deserialize for PostgresqlAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<PostgresqlAppenderField, D::Error>
            where D: Deserializer
        {
            struct PostgresqlAppenderFieldVisitor;

            impl Visitor for PostgresqlAppenderFieldVisitor {
                type Value = PostgresqlAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<PostgresqlAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(PostgresqlAppenderField::Name),
                        "database" => Ok(PostgresqlAppenderField::Database),
                        "filter" => Ok(PostgresqlAppenderField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(PostgresqlAppenderFieldVisitor)
        }
    }

    impl Deserialize for PostgresqlAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<PostgresqlAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "database", "filter"];
            deserializer.visit_struct("PostgresqlAppender", FIELDS, PostgresqlAppenderVisitor)
        }
    }

    struct PostgresqlAppenderVisitor;

    impl Visitor for PostgresqlAppenderVisitor {
        type Value = PostgresqlAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<PostgresqlAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut database: Option<String> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(PostgresqlAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(PostgresqlAppenderField::Database) => {
                        database = Some(try!(visitor.visit_value()));
                    }
                    Some(PostgresqlAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let db = match database {
                Some(d) => d,
                None => return visitor.missing_field("database"),
            };

            try!(visitor.end());

            Ok(PostgresqlAppender::new().name(nm).database(db).filter(filter))
        }
    }
}
