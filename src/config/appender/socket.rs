//! Socket Appender
use {Append, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::error::Error;
use std::io::Write;
use std::net::TcpStream;

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[derive(Debug, Default)]
/// Socket Appender
pub struct SocketAppender {
    /// Appender name.
    name: String,
    /// Socket Address.
    socket_addr: String,
    /// Filters associated with the appender.
    filter: Option<FilterType>,
}

impl SocketAppender {
    /// Create a new default SocketAppender.
    pub fn new() -> SocketAppender {
        Default::default()
    }

    /// Set the name of the SocketAppender.
    pub fn name(mut self, name: String) -> SocketAppender {
        self.name = name;
        self
    }

    /// Set the socket address of the SocketAppender.
    pub fn socket_addr(mut self, socket_addr: String) -> SocketAppender {
        self.socket_addr = socket_addr;
        self
    }

    /// Set the optional filters for the SocketAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> SocketAppender {
        self.filter = filter;
        self
    }
}

impl Named for SocketAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for SocketAppender {
    fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>> {
        let mut stream = try!(TcpStream::connect(&self.socket_addr[..]));
        try!(stream.write_fmt(*record.args()));
        Ok(())
    }
}

impl Into<Appender> for SocketAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{Error, MapVisitor, Visitor};

    enum SocketAppenderField {
        Name,
        SocketAddress,
        Filter,
    }

    impl Deserialize for SocketAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<SocketAppenderField, D::Error>
            where D: Deserializer
        {
            struct SocketAppenderFieldVisitor;

            impl Visitor for SocketAppenderFieldVisitor {
                type Value = SocketAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<SocketAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(SocketAppenderField::Name),
                        "socket_addr" => Ok(SocketAppenderField::SocketAddress),
                        "filter" => Ok(SocketAppenderField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(SocketAppenderFieldVisitor)
        }
    }

    impl Deserialize for SocketAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<SocketAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "socket_addr", "filter"];
            deserializer.visit_struct("SocketAppender", FIELDS, SocketAppenderVisitor)
        }
    }

    struct SocketAppenderVisitor;

    impl Visitor for SocketAppenderVisitor {
        type Value = SocketAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<SocketAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut socket_addr: Option<String> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(SocketAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(SocketAppenderField::SocketAddress) => {
                        socket_addr = Some(try!(visitor.visit_value()));
                    }
                    Some(SocketAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let sa = match socket_addr {
                Some(s) => s,
                None => return visitor.missing_field("socket_addr"),
            };

            try!(visitor.end());

            Ok(SocketAppender::new()
                   .name(nm)
                   .socket_addr(sa)
                   .filter(filter))
        }
    }
}

#[cfg(test)]
mod test {
    use {Named, decode};
    use super::*;

    const BASE_CONFIG: &'static str = r#"
name = "socket"
socket_addr = "127.0.0.1:12345"
    "#;
    const ALL_CONFIG: &'static str = r#"
name = "socket"
socket_addr = "127.0.0.1:12345"

[filter.threshold]
level = "Warn"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
blah = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
name = 1
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
name = "socket"
filter = 1
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
name = "socket"
socket_addr = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4];

    #[test]
    fn test_default() {
        let def: SocketAppender = Default::default();
        assert!(Named::name(&def).is_empty());
        assert!(def.socket_addr.is_empty());
        assert!(def.filter.is_none());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<SocketAppender>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<SocketAppender>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
