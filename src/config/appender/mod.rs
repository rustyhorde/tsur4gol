//! tsur4gol Appender
use {Append, Filter, Named};
use config::filter::MatchAction::*;
#[cfg(feature = "mysql")]
use config::appender::db::mysql::MysqlAppender;
#[cfg(feature = "postgresql")]
use config::appender::db::postgresql::PostgresqlAppender;
#[cfg(feature = "socket")]
use config::appender::socket::SocketAppender;
#[cfg(feature = "sqlite")]
use config::appender::db::sqlite::SqliteAppender;
use log::LogRecord;
use std::default::Default;
use std::error::Error;
use std::fmt;

#[cfg(any(feature = "mysql", feature = "postgresql", features = "sqlite"))]
pub mod db;
pub mod noop;
pub mod file;
#[cfg(feature = "socket")]
pub mod socket;
pub mod stdx;

/// Appender struct for a given Append and set of Filter.
pub struct Appender {
    /// The appender.
    pub appender: Box<Append>,
    /// The filters associated with the appender.
    pub filters: Vec<Box<Filter>>,
}

impl fmt::Debug for Appender {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Appender")
         .field("appender", &self.appender.name())
         .field("filters", &self.filters.len())
         .finish()
    }
}

impl Appender {
    /// Check the filters and if they pass, call the specific appender implementation.
    pub fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>> {
        for filter in &mut self.filters {
            match filter.filter(record) {
                Accept => break,
                Neutral => {}
                Deny => return Ok(()),
            }
        }

        self.appender.append(record)
    }

    /// Get the name for the appender.
    pub fn name(&self) -> String {
        self.appender.name()
    }
}

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[derive(Debug)]
/// Appender Struct
pub struct AppenderType {
    /// noop Appenders
    noop: Option<Vec<noop::NoopAppender>>,
    /// stdout Appenders
    stdout: Option<Vec<stdx::StdoutAppender>>,
    /// stderr Appenders
    stderr: Option<Vec<stdx::StderrAppender>>,
    /// file Appenders
    file: Option<Vec<file::FileAppender>>,
    #[cfg(feature = "mysql")]
    /// MySQL Appenders
    pub mysql: Option<Vec<MysqlAppender>>,
    #[cfg(not(feature = "mysql"))]
    /// Noop Appender if mysql not configured.
    pub mysql: Option<Vec<noop::NoopAppender>>,
    #[cfg(feature = "postgresql")]
    /// PostgreSQL Appenders
    pub postgresql: Option<Vec<PostgresqlAppender>>,
    #[cfg(not(feature = "postgresql"))]
    /// Noop Appender if postgresql not configured.
    pub postgresql: Option<Vec<noop::NoopAppender>>,
    #[cfg(feature = "socket")]
    /// Socket Appenders
    pub socket: Option<Vec<SocketAppender>>,
    #[cfg(not(feature = "socket"))]
    /// Noop Appender if socket not configured.
    pub socket: Option<Vec<noop::NoopAppender>>,
    #[cfg(feature = "sqlite")]
    /// Sqlite Appenders
    pub sqlite: Option<Vec<SqliteAppender>>,
    #[cfg(not(feature = "sqlite"))]
    /// Noop Appender if socket not configured.
    pub sqlite: Option<Vec<noop::NoopAppender>>,
}

fn get_appender_names<T>(names: &mut Vec<String>, app: &Option<Vec<T>>)
    where T: Named
{
    if let Some(ref s) = *app {
        for a in s.iter() {
            names.push(a.name());
        }
    }
}

impl AppenderType {
    /// Create a new default AppenderType.
    pub fn new() -> AppenderType {
        Default::default()
    }

    /// Add an optional vector of noop appenders to the AppenderType.
    pub fn noop(mut self, noop: Option<Vec<noop::NoopAppender>>) -> AppenderType {
        self.noop = noop;
        self
    }

    /// Add an optional vector of stdout appenders to the AppenderType.
    pub fn stdout(mut self, stdout: Option<Vec<stdx::StdoutAppender>>) -> AppenderType {
        self.stdout = stdout;
        self
    }

    /// Add an optional vector of stderr appenders to the AppenderType.
    pub fn stderr(mut self, stderr: Option<Vec<stdx::StderrAppender>>) -> AppenderType {
        self.stderr = stderr;
        self
    }

    /// Add an optional vector of file appenders to the AppenderType.
    pub fn file(mut self, file: Option<Vec<file::FileAppender>>) -> AppenderType {
        self.file = file;
        self
    }

    #[cfg(feature = "mysql")]
    /// Add an optional vector of mysql appenders to the AppenderType.
    pub fn mysql(mut self, mysql: Option<Vec<MysqlAppender>>) -> AppenderType {
        self.mysql = mysql;
        self
    }

    #[cfg(not(feature = "mysql"))]
    /// Add an optional vector of noop appenders to the AppenderType (mysql is not configured).
    pub fn mysql(mut self, mysql: Option<Vec<noop::NoopAppender>>) -> AppenderType {
        self.mysql = mysql;
        self
    }

    #[cfg(feature = "postgresql")]
    /// Add an optional vector of postgresql appenders to the AppenderType.
    pub fn postgresql(mut self, postgresql: Option<Vec<PostgresqlAppender>>) -> AppenderType {
        self.postgresql = postgresql;
        self
    }

    #[cfg(not(feature = "postgresql"))]
    /// Add an optional vector of noop appenders to the AppenderType (postgresql is not configured).
    pub fn postgresql(mut self, postgresql: Option<Vec<noop::NoopAppender>>) -> AppenderType {
        self.postgresql = postgresql;
        self
    }

    #[cfg(feature = "socket")]
    /// Add an optional vector of socket appenders to the AppenderType.
    pub fn socket(mut self, socket: Option<Vec<SocketAppender>>) -> AppenderType {
        self.socket = socket;
        self
    }

    #[cfg(not(feature = "socket"))]
    /// Add an optional vector of noop appenders to the AppenderType (postgresql is not configured).
    pub fn socket(mut self, socket: Option<Vec<noop::NoopAppender>>) -> AppenderType {
        self.socket = socket;
        self
    }

    #[cfg(feature = "sqlite")]
    /// Add an optional vector of socket appenders to the AppenderType.
    pub fn sqlite(mut self, sqlite: Option<Vec<SqliteAppender>>) -> AppenderType {
        self.sqlite = sqlite;
        self
    }

    #[cfg(not(feature = "sqlite"))]
    /// Add an optional vector of noop appenders to the AppenderType (sqlite is not configured).
    pub fn sqlite(mut self, sqlite: Option<Vec<noop::NoopAppender>>) -> AppenderType {
        self.sqlite = sqlite;
        self
    }

    /// Get the appender names.
    pub fn get_names(&self) -> Vec<String> {
        let mut names = Vec::new();

        get_appender_names(&mut names, &self.noop);
        get_appender_names(&mut names, &self.stdout);
        get_appender_names(&mut names, &self.stderr);
        get_appender_names(&mut names, &self.file);
        self.get_mysql_names(&mut names);
        self.get_postgresql_names(&mut names);
        self.get_socket_names(&mut names);
        self.get_sqlite_names(&mut names);
        names
    }

    #[cfg(feature = "mysql")]
    fn get_mysql_names(&self, names: &mut Vec<String>) {
        get_appender_names(names, &self.mysql);
    }

    #[cfg(not(feature = "mysql"))]
    fn get_mysql_names(&self, _: &mut Vec<String>) {}

    #[cfg(feature = "postgresql")]
    fn get_postgresql_names(&self, names: &mut Vec<String>) {
        get_appender_names(names, &self.postgresql);
    }

    #[cfg(not(feature = "postgresql"))]
    fn get_postgresql_names(&self, _: &mut Vec<String>) {}

    #[cfg(feature = "socket")]
    fn get_socket_names(&self, names: &mut Vec<String>) {
        get_appender_names(names, &self.socket);
    }

    #[cfg(not(feature = "socket"))]
    fn get_socket_names(&self, _: &mut Vec<String>) {}

    #[cfg(feature = "sqlite")]
    fn get_sqlite_names(&self, names: &mut Vec<String>) {
        get_appender_names(names, &self.sqlite);
    }

    #[cfg(not(feature = "sqlite"))]
    fn get_sqlite_names(&self, _: &mut Vec<String>) {}
}

#[cfg(feature = "mysql")]
fn add_mysql(a: &mut AppenderType) {
    a.mysql = None;
}

#[cfg(not(feature = "mysql"))]
fn add_mysql(_: &mut AppenderType) {}

#[cfg(feature = "postgresql")]
fn add_postgresql(a: &mut AppenderType) {
    a.postgresql = None;
}

#[cfg(not(feature = "postgresql"))]
fn add_postgresql(_: &mut AppenderType) {}

#[cfg(feature = "socket")]
fn add_socket(a: &mut AppenderType) {
    a.socket = None;
}

#[cfg(not(feature = "socket"))]
fn add_socket(_: &mut AppenderType) {}

#[cfg(feature = "sqlite")]
fn add_sqlite(a: &mut AppenderType) {
    a.sqlite = None;
}

#[cfg(not(feature = "sqlite"))]
fn add_sqlite(_: &mut AppenderType) {}

impl Default for AppenderType {
    fn default() -> AppenderType {
        let mut a = AppenderType {
            noop: None,
            stdout: Some(vec![Default::default()]),
            stderr: None,
            file: None,
            mysql: None,
            postgresql: None,
            socket: None,
            sqlite: None,
        };

        add_mysql(&mut a);
        add_postgresql(&mut a);
        add_sqlite(&mut a);
        add_socket(&mut a);

        a
    }
}

impl Into<Vec<Appender>> for AppenderType {
    fn into(self) -> Vec<Appender> {
        let mut appenders: Vec<Appender> = Vec::new();

        if let Some(noops) = self.noop {
            for a in noops {
                appenders.push(a.into());
            }
        }

        if let Some(stdouts) = self.stdout {
            for a in stdouts {
                appenders.push(a.into());
            }
        }

        if let Some(stderrs) = self.stderr {
            for a in stderrs {
                appenders.push(a.into());
            }
        }

        if let Some(files) = self.file {
            for a in files {
                appenders.push(a.into());
            }
        }

        if cfg!(feature = "mysql") {
            if let Some(mysqls) = self.mysql {
                for a in mysqls {
                    appenders.push(a.into());
                }
            }
        }

        if cfg!(feature = "postgresql") {
            if let Some(postgres) = self.postgresql {
                for a in postgres {
                    appenders.push(a.into());
                }
            }
        }

        if cfg!(feature = "socket") {
            if let Some(sockets) = self.socket {
                for a in sockets {
                    appenders.push(a.into());
                }
            }
        }

        if cfg!(feature = "sqlite") {
            if let Some(sqlites) = self.sqlite {
                for a in sqlites {
                    appenders.push(a.into());
                }
            }
        }

        appenders
    }
}

#[cfg(feature = "serde")]
mod serde {
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};
    use super::*;

    enum AppenderTypeField {
        Noop,
        Stdout,
        Stderr,
        File,
        Mysql,
        Postgresql,
        Socket,
        Sqlite,
    }

    impl Deserialize for AppenderTypeField {
        fn deserialize<D>(deserializer: &mut D) -> Result<AppenderTypeField, D::Error>
            where D: Deserializer
        {
            struct AppenderTypeFieldVisitor;

            impl Visitor for AppenderTypeFieldVisitor {
                type Value = AppenderTypeField;

                fn visit_str<E>(&mut self, value: &str) -> Result<AppenderTypeField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "noop" => Ok(AppenderTypeField::Noop),
                        "stdout" => Ok(AppenderTypeField::Stdout),
                        "stderr" => Ok(AppenderTypeField::Stderr),
                        "file" => Ok(AppenderTypeField::File),
                        "mysql" => Ok(AppenderTypeField::Mysql),
                        "postgresql" => Ok(AppenderTypeField::Postgresql),
                        "socket" => Ok(AppenderTypeField::Socket),
                        "sqlite" => Ok(AppenderTypeField::Sqlite),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(AppenderTypeFieldVisitor)
        }
    }

    impl Deserialize for AppenderType {
        fn deserialize<D>(deserializer: &mut D) -> Result<AppenderType, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["noop",
                                                       "stdout",
                                                       "stderr",
                                                       "file",
                                                       "mysql",
                                                       "postgresql",
                                                       "socket",
                                                       "sqlite"];
            deserializer.visit_struct("Appender", FIELDS, AppenderTypeVisitor)
        }
    }

    struct AppenderTypeVisitor;

    impl Visitor for AppenderTypeVisitor {
        type Value = AppenderType;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<AppenderType, V::Error>
            where V: MapVisitor
        {
            let mut noop: Option<Vec<noop::NoopAppender>> = None;
            let mut stdout: Option<Vec<stdx::StdoutAppender>> = None;
            let mut stderr: Option<Vec<stdx::StderrAppender>> = None;
            let mut file: Option<Vec<file::FileAppender>> = None;
            let mut mysql = None;
            let mut postgresql = None;
            let mut socket = None;
            let mut sqlite = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(AppenderTypeField::Noop) => {
                        noop = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Stdout) => {
                        stdout = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Stderr) => {
                        stderr = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::File) => {
                        file = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Mysql) => {
                        mysql = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Postgresql) => {
                        postgresql = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Socket) => {
                        socket = Some(try!(visitor.visit_value()));
                    }
                    Some(AppenderTypeField::Sqlite) => {
                        sqlite = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            try!(visitor.end());

            Ok(AppenderType::new()
                   .noop(noop)
                   .stdout(stdout)
                   .stderr(stderr)
                   .file(file)
                   .mysql(mysql)
                   .postgresql(postgresql)
                   .socket(socket)
                   .sqlite(sqlite))
        }
    }
}
