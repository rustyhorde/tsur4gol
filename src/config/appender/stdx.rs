//! stdx Appender Impementations
use {Append, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::default::Default;
use std::error::Error;
use std::io::{self, Write};

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug)]
/// stdout Appender
pub struct StdoutAppender {
    /// Appender name.
    name: String,
    /// Filters associated with the appender.
    filter: Option<FilterType>,
    /// Output pattern.
    pattern: Option<String>,
}

impl Default for StdoutAppender {
    fn default() -> StdoutAppender {
        StdoutAppender {
            name: "console".to_owned(),
            filter: None,
            pattern: Some("%m%n".to_owned()),
        }
    }
}

impl StdoutAppender {
    /// Create a new default StdoutAppender.
    pub fn new() -> StdoutAppender {
        Default::default()
    }

    /// Set the name of the StdoutAppender.
    pub fn name(mut self, name: String) -> StdoutAppender {
        self.name = name;
        self
    }

    /// Set the optional filters for the StdoutAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> StdoutAppender {
        self.filter = filter;
        self
    }

    /// Set the optional output pattern for the StdoutAppender.
    pub fn pattern(mut self, pattern: Option<String>) -> StdoutAppender {
        self.pattern = pattern;
        self
    }
}

impl Named for StdoutAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for StdoutAppender {
    fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>> {
        let stdout = io::stdout();
        let mut lck = stdout.lock();
        try!(lck.write_fmt(format_args!("{}\n", record.args())));
        try!(lck.flush());
        Ok(())
    }
}

impl Into<Appender> for StdoutAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug, Default)]
/// stderr Appender
pub struct StderrAppender {
    /// Appender name.
    name: String,
    /// Filters associated with the appender.
    filter: Option<FilterType>,
    /// Output pattern.
    pattern: Option<String>,
}

impl StderrAppender {
    /// Create a new default StderrAppender.
    pub fn new() -> StderrAppender {
        Default::default()
    }

    /// Set the name of the StderrAppender.
    pub fn name(mut self, name: String) -> StderrAppender {
        self.name = name;
        self
    }

    /// Set the optional filters for the StderrAppender.
    pub fn filter(mut self, filter: Option<FilterType>) -> StderrAppender {
        self.filter = filter;
        self
    }

    /// Set the optional output pattern for the StdoutStderrAppenderAppender.
    pub fn pattern(mut self, pattern: Option<String>) -> StderrAppender {
        self.pattern = pattern;
        self
    }
}

impl Named for StderrAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for StderrAppender {
    fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>> {
        let stderr = io::stderr();
        let mut lck = stderr.lock();
        try!(lck.write_fmt(format_args!("{}\n", record.args())));
        try!(lck.flush());
        Ok(())
    }
}

impl Into<Appender> for StderrAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum StdxAppenderField {
        Name,
        Filter,
        Pattern,
    }

    impl Deserialize for StdxAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<StdxAppenderField, D::Error>
            where D: Deserializer
        {
            struct StdxAppenderFieldVisitor;

            impl Visitor for StdxAppenderFieldVisitor {
                type Value = StdxAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<StdxAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(StdxAppenderField::Name),
                        "filter" => Ok(StdxAppenderField::Filter),
                        "pattern" => Ok(StdxAppenderField::Pattern),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(StdxAppenderFieldVisitor)
        }
    }

    impl Deserialize for StdoutAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<StdoutAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "filter", "pattern"];
            deserializer.visit_struct("StdoutAppender", FIELDS, StdoutAppenderVisitor)
        }
    }

    struct StdoutAppenderVisitor;

    impl Visitor for StdoutAppenderVisitor {
        type Value = StdoutAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<StdoutAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut filter: Option<FilterType> = None;
            let mut pattern: Option<String> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(StdxAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(StdxAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    Some(StdxAppenderField::Pattern) => {
                        pattern = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            try!(visitor.end());

            Ok(StdoutAppender::new().name(nm).filter(filter).pattern(pattern))
        }
    }

    impl Deserialize for StderrAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<StderrAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "filter", "pattern"];
            deserializer.visit_struct("StderrAppender", FIELDS, StderrAppenderVisitor)
        }
    }

    struct StderrAppenderVisitor;

    impl Visitor for StderrAppenderVisitor {
        type Value = StderrAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<StderrAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut filter: Option<FilterType> = None;
            let mut pattern: Option<String> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(StdxAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(StdxAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    Some(StdxAppenderField::Pattern) => {
                        pattern = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            try!(visitor.end());

            Ok(StderrAppender::new().name(nm).filter(filter).pattern(pattern))
        }
    }
}

#[cfg(test)]
mod test {
    pub use super::*;

    mod stdout {
        use {Named, decode};
        use super::*;

        const BASE_CONFIG: &'static str = r#"
    name = "console"
        "#;
        const ALL_CONFIG: &'static str = r#"
    name = "console"
    pattern = "%m%n"

    [filter.threshold]
    level = "Info"
        "#;

        static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

        const INVALID_CONFIG_0: &'static str = r#""#;
        const INVALID_CONFIG_1: &'static str = r#"
        blah = 1
        "#;
        const INVALID_CONFIG_2: &'static str = r#"
    name = 1
        "#;
        const INVALID_CONFIG_3: &'static str = r#"
    name = "console"
    pattern = 1
        "#;
        const INVALID_CONFIG_4: &'static str = r#"
    name = "console"
    pattern = "%m%n"

    [filter.blah]
    name = "blah"
        "#;

        static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                     INVALID_CONFIG_1,
                                                     INVALID_CONFIG_2,
                                                     INVALID_CONFIG_3,
                                                     INVALID_CONFIG_4];

        #[test]
        fn test_default() {
            let sa: StdoutAppender = Default::default();
            assert!(Named::name(&sa) == "console");
            assert!(sa.filter.is_none());
            assert!(sa.pattern == Some("%m%n".to_owned()));
        }

        #[test]
        fn test_valid_configs() {
            let mut results = Vec::new();

            for valid in VALIDS {
                match decode::<StdoutAppender>(valid) {
                    Ok(_) => {
                        results.push(true);
                    }
                    Err(_) => {
                        assert!(false);
                    }
                };
            }

            assert!(results.iter().all(|x| *x));
        }

        #[test]
        fn test_invalid_configs() {
            let mut results = Vec::new();

            for invalid in INVALIDS {
                match decode::<StdoutAppender>(invalid) {
                    Ok(_) => {
                        assert!(false);
                    }
                    Err(_) => {
                        results.push(true);
                    }
                };
            }

            assert!(results.iter().all(|x| *x));
        }
    }

    mod stderr {
        use {Named, decode};
        use super::*;

        const BASE_CONFIG: &'static str = r#"
    name = "console"
        "#;
        const ALL_CONFIG: &'static str = r#"
    name = "console"
    pattern = "%m%n"

    [filter.threshold]
    level = "Info"
        "#;

        static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

        const INVALID_CONFIG_0: &'static str = r#""#;
        const INVALID_CONFIG_1: &'static str = r#"
        blah = 1
        "#;
        const INVALID_CONFIG_2: &'static str = r#"
    name = 1
        "#;
        const INVALID_CONFIG_3: &'static str = r#"
    name = "console"
    pattern = 1
        "#;
        const INVALID_CONFIG_4: &'static str = r#"
    name = "console"
    pattern = "%m%n"

    [filter.blah]
    name = "blah"
        "#;

        static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                     INVALID_CONFIG_1,
                                                     INVALID_CONFIG_2,
                                                     INVALID_CONFIG_3,
                                                     INVALID_CONFIG_4];

        #[test]
        fn test_default() {
            let sa: StderrAppender = Default::default();
            assert!(Named::name(&sa).is_empty());
            assert!(sa.filter.is_none());
            assert!(sa.pattern.is_none());
        }

        #[test]
        fn test_valid_configs() {
            let mut results = Vec::new();

            for valid in VALIDS {
                match decode::<StderrAppender>(valid) {
                    Ok(_) => {
                        results.push(true);
                    }
                    Err(_) => {
                        assert!(false);
                    }
                };
            }

            assert!(results.iter().all(|x| *x));
        }

        #[test]
        fn test_invalid_configs() {
            let mut results = Vec::new();

            for invalid in INVALIDS {
                match decode::<StderrAppender>(invalid) {
                    Ok(_) => {
                        assert!(false);
                    }
                    Err(_) => {
                        results.push(true);
                    }
                };
            }

            assert!(results.iter().all(|x| *x));
        }
    }
}
