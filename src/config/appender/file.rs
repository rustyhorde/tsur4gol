//! File Appender
use {Append, Filter, Named};
use config::appender::Appender;
use config::filter::FilterType;
use log::LogRecord;
use std::error::Error;
use std::fmt;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

/// FileAppender Struct
pub struct FileAppender {
    /// Appender name.
    name: String,
    /// The path to the file where log events should be written.
    path: PathBuf,
    /// Set to true if you would rather truncate the file before writing, rather than appending.
    truncate: Option<bool>,
    /// Filter associated with this appender.
    filter: Option<FilterType>,
    /// OpenOptions for writing to file.
    oo: Arc<Mutex<OpenOptions>>,
}

impl Default for FileAppender {
    fn default() -> FileAppender {
        use std::env;

        FileAppender {
            name: String::new(),
            path: env::temp_dir().join("tsurgol.log"),
            truncate: None,
            filter: None,
            oo: Arc::new(Mutex::new(OpenOptions::new())),
        }
    }
}

impl fmt::Debug for FileAppender {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("FileAppender")
         .field("name", &self.name)
         .field("path", &self.path)
         .field("truncate", &self.truncate)
         .finish()
    }
}

impl FileAppender {
    /// Create a new default FileAppender.
    pub fn new() -> FileAppender {
        Default::default()
    }

    /// Set the name of the file appender.
    pub fn name(mut self, name: String) -> FileAppender {
        self.name = name;
        self
    }

    /// Set the file thate will be written to.
    pub fn path(mut self, path: PathBuf) -> FileAppender {
        self.path = path;
        self
    }

    /// Set the truncate flag.
    pub fn truncate(mut self, truncate: Option<bool>) -> FileAppender {
        self.truncate = truncate;
        self
    }

    /// Set the OpenOptions
    pub fn oo(mut self, oo: Arc<Mutex<OpenOptions>>) -> FileAppender {
        self.oo = oo;
        self
    }

    /// An optional list of filter to be applied at the appender level.
    pub fn filter(mut self, filter: Option<FilterType>) -> FileAppender {
        self.filter = filter;
        self
    }
}

impl Named for FileAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for FileAppender {
    fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>> {
        if let Ok(mut lck) = self.oo.lock() {
            if let Ok(mut f) = lck.open(&self.path) {
                try!(f.write_fmt(*record.args()));
                try!(f.write(b"\n"));
                try!(f.flush());
                // Reset the truncate and append options, so truncate only happens once.
                lck.truncate(false).append(true);
            }
        }
        Ok(())
    }
}

impl Into<Appender> for FileAppender {
    fn into(self) -> Appender {
        let fv = match self.filter {
            Some(ref f) => f.clone().into(),
            None => Vec::new(),
        };

        Appender {
            appender: Box::new(self),
            filters: fv,
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use rustc_serialize::{Decodable, Decoder};
    use std::path::PathBuf;
    use super::*;

    impl Decodable for FileAppender {
        fn decode<D: Decoder>(d: &mut D) -> Result<FileAppender, D::Error> {
            d.read_struct("FileAppender", 4, |d| {
                let name = try!(d.read_struct_field("name", 1, |d| Decodable::decode(d)));
                let pathstr = try!(d.read_struct_field("path", 2, |d| Decodable::decode(d)));
                let truncate = try!(d.read_struct_field("truncate", 3, |d| Decodable::decode(d)));
                let filter = try!(d.read_struct_field("filters", 4, |d| Decodable::decode(d)));
                let path = PathBuf::from(pathstr);
                let mut oo = OpenOptions::new();
                oo.create(true).write(true);

                match truncate {
                    Some(t) => {
                        if t {
                            oo.truncate(true);
                        } else {
                            oo.append(true);
                        }
                    }
                    None => {
                        oo.append(true);
                    }
                }

                Ok(FileAppender::new()
                       .name(nm)
                       .path(p)
                       .truncate(truncate)
                       .filter(filter)
                       .oo(Arc::new(Mutex::new(oo))))
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::filter::FilterType;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};
    use std::fs::OpenOptions;
    use std::path::PathBuf;
    use std::sync::{Arc, Mutex};

    enum FileAppenderField {
        Name,
        FPath,
        Truncate,
        Filter,
    }

    impl Deserialize for FileAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<FileAppenderField, D::Error>
            where D: Deserializer
        {
            struct FileAppenderFieldVisitor;

            impl Visitor for FileAppenderFieldVisitor {
                type Value = FileAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<FileAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(FileAppenderField::Name),
                        "path" => Ok(FileAppenderField::FPath),
                        "truncate" => Ok(FileAppenderField::Truncate),
                        "filter" => Ok(FileAppenderField::Filter),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(FileAppenderFieldVisitor)
        }
    }

    impl Deserialize for FileAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<FileAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name", "path", "truncate", "filter"];
            deserializer.visit_struct("FileAppender", FIELDS, FileAppenderVisitor)
        }
    }

    struct FileAppenderVisitor;

    impl Visitor for FileAppenderVisitor {
        type Value = FileAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<FileAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut path: Option<PathBuf> = None;
            let mut truncate: Option<bool> = None;
            let mut filter: Option<FilterType> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(FileAppenderField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(FileAppenderField::FPath) => {
                        path = Some(try!(visitor.visit_value()));
                    }
                    Some(FileAppenderField::Truncate) => {
                        truncate = Some(try!(visitor.visit_value()));
                    }
                    Some(FileAppenderField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let p = match path {
                Some(pa) => pa,
                None => return visitor.missing_field("path"),
            };

            let mut oo = OpenOptions::new();
            oo.create(true).write(true);

            match truncate {
                Some(t) => {
                    if t {
                        oo.truncate(true);
                    } else {
                        oo.append(true);
                    }
                }
                None => {
                    oo.append(true);
                }
            }

            try!(visitor.end());

            Ok(FileAppender::new()
                   .name(nm)
                   .path(p)
                   .truncate(truncate)
                   .filter(filter)
                   .oo(Arc::new(Mutex::new(oo))))
        }
    }
}

#[cfg(test)]
mod test {
    use {Named, decode};
    use std::env;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
name = "file"
path = "a/path/to/somewhere"
    "#;
    const ALL_CONFIG: &'static str = r#"
name = "file"
path = "a/path/to/nowhere"

[filter.threshold]
level = "Warn"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
name = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
name = "no path"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
yoda = "log, you will"
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
name = "file"
path = 1
    "#;
    const INVALID_CONFIG_5: &'static str = r#"
name = "file"
path = "some/path"
filter = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4,
                                                 INVALID_CONFIG_5];

    #[test]
    fn test_default() {
        let def: FileAppender = Default::default();
        assert!(Named::name(&def).is_empty());
        assert!(def.path == env::temp_dir().join("tsurgol.log"));
        assert!(def.filter.is_none());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<FileAppender>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => {
                    assert!(false);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<FileAppender>(invalid) {
                Ok(_) => {
                    assert!(false);
                }
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
