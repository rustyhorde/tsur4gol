//! File Appender
use {Append, Named};
use config::appender::Appender;
use log::LogRecord;
use std::error::Error;

#[cfg_attr(feature = "rustc-serialize", derive(RustcDecodable))]
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug, Default)]
/// Noop Appender
pub struct NoopAppender {
    /// Appender name.
    pub name: String,
}

impl NoopAppender {
    /// Create a new default NoopAppender.
    pub fn new() -> NoopAppender {
        Default::default()
    }

    /// Set the name of the NoopAppender.
    pub fn name(mut self, name: String) -> NoopAppender {
        self.name = name;
        self
    }
}

impl Named for NoopAppender {
    fn name(&self) -> String {
        self.name.clone()
    }
}

impl Append for NoopAppender {
    fn append(&mut self, _record: &LogRecord) -> Result<(), Box<Error>> {
        Ok(())
    }
}

impl Into<Appender> for NoopAppender {
    fn into(self) -> Appender {
        Appender {
            appender: Box::new(self),
            filters: Vec::new(),
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum NoopAppenderField {
        Name,
    }

    impl Deserialize for NoopAppenderField {
        fn deserialize<D>(deserializer: &mut D) -> Result<NoopAppenderField, D::Error>
            where D: Deserializer
        {
            struct NoopAppenderFieldVisitor;

            impl Visitor for NoopAppenderFieldVisitor {
                type Value = NoopAppenderField;

                fn visit_str<E>(&mut self, value: &str) -> Result<NoopAppenderField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(NoopAppenderField::Name),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(NoopAppenderFieldVisitor)
        }
    }

    impl Deserialize for NoopAppender {
        fn deserialize<D>(deserializer: &mut D) -> Result<NoopAppender, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name"];
            deserializer.visit_struct("NoopAppender", FIELDS, NoopAppenderVisitor)
        }
    }

    struct NoopAppenderVisitor;

    impl Visitor for NoopAppenderVisitor {
        type Value = NoopAppender;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<NoopAppender, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;

            while let Some(NoopAppenderField::Name) = try!(visitor.visit_key()) {
                name = Some(try!(visitor.visit_value()));
            }


            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            try!(visitor.end());

            Ok(NoopAppender::new().name(nm))
        }
    }
}

#[cfg(test)]
mod test {
    use {Named, decode};
    use super::*;

    const BASE_CONFIG: &'static str = r#"
name = "noop"
    "#;
    const ALL_CONFIG: &'static str = r#"
name = "noop"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
    blah = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
name = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2];

    #[test]
    fn test_default() {
        let def: NoopAppender = Default::default();
        assert!(Named::name(&def).is_empty());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<NoopAppender>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => {
                    assert!(false);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<NoopAppender>(invalid) {
                Ok(_) => {
                    assert!(false);
                }
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
