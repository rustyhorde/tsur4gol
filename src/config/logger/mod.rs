//! Logger Configuration
//! [loglevelfilter]: http://doc.rust-lang.org/log/log/enum.LogLevelFilter.html
//!
//! ## Named Hierarchy
//! A Logger is said to be an ancestor of another Logger if its name followed by a '::' is a prefix
//! of the descendant logger name. A Logger is said to be a parent of a child Logger if there are no
//! ancestors between itself and the descendant Logger.
//!
//! ### Examples
//!
//! * ```tsur4gol::config::logger``` is a descendant of the ```root``` logger.
//! * ```tsur4gol::config``` is the parent (and ancestor) of ```tsur4gol::config::logger```
//! * ```root``` is the ancestor of all other loggers.  Put another way, every logger is a
//! descendant of the root logger.
//!
//! A Logger is configured using the logger element. The logger element must have a name attribute
//! specified, will usually have a level attribute specified and may also have an additivity
//! attribute specified. The level may be configured with one of *Trace*, *Debug*, *Info*, *Warn*,
//! *Error*, or *Off* (see [LogLevelFilter]).  If no level is specified it will default to *Error*.
//! The additivity attribute may be assigned a value of true or false. If the attribute is omitted
//! the default value of false will be used.
//!
//! The Logger may also be configured with one or more AppenderRef elements. Each appender
//! referenced will become associated with the specified Logger. If multiple appenders are
//! configured on the Logger each of them be called when processing logging events.
//!
//! Every configuration must have a 'root' logger. If one is not configured the default root Logger,
//! which has a level of Error and has a stdout appender ref named 'console', will be used. The main
//! differences between the root logger and other loggers are:
//!
//! * The root logger has no parent loggers.
//! * The root logger does not support the additivity attribute since it has no parent.
//!
//! ## Sample Configuration
//!
//! ```toml
//! [[logger]]
//! name = "root"
//!
//! [[logger]]
//! name = "tsur4gol::config::logger"
//! level = "Trace"
//! additive = false
//! ```
use config::appender_ref::AppenderRefType;
use config::filter::FilterType;
use log::LogLevelFilter;
use std::default::Default;

#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug)]
/// Logger struct
pub struct Logger {
    /// Logger name
    name: String,
    /// Optional LogLevelFilter level
    level: Option<LogLevelFilter>,
    /// Optional Appender reference
    appender_ref: Option<Vec<AppenderRefType>>,
    /// Optional filter
    filter: Option<FilterType>,
    /// Optional additivity flag
    additive: Option<bool>,
}

impl Logger {
    /// Create a new default Logger.
    pub fn new() -> Logger {
        Default::default()
    }

    /// Set the Logger name.
    pub fn set_name(mut self, name: String) -> Logger {
        self.name = name;
        self
    }

    /// Set the Logger LogLevelFilter.
    pub fn set_level(mut self, level: Option<LogLevelFilter>) -> Logger {
        self.level = level;
        self
    }

    /// Set the Logger AppenderRef vector.
    pub fn set_appender_ref(mut self, appender_ref: Option<Vec<AppenderRefType>>) -> Logger {
        self.appender_ref = appender_ref;
        self
    }

    /// Set the Logger filter.
    pub fn set_filter(mut self, filter: Option<FilterType>) -> Logger {
        self.filter = filter;
        self
    }

    /// Set the Logger additive flag.
    pub fn set_additive(mut self, additive: Option<bool>) -> Logger {
        self.additive = additive;
        self
    }

    /// Get the Logger name.
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Get the additive value for this logger.  Defaults to true.
    pub fn additive(&self) -> Option<bool> {
        self.additive
    }

    /// Get the LogLevelFilter for this logger.  Defaults to Error.
    pub fn level(&self) -> Option<LogLevelFilter> {
        self.level
    }

    /// Get the filter for this Logger.
    pub fn filter(&self) -> Option<FilterType> {
        self.filter.clone()
    }

    /// Get the AppenderRef for this logger.
    pub fn appender_ref(&self) -> Option<Vec<AppenderRefType>> {
        self.appender_ref.clone()
    }
}

impl Default for Logger {
    fn default() -> Logger {
        let mut arefs = Vec::new();
        let def_ref: AppenderRefType = Default::default();
        arefs.push(def_ref);

        Logger {
            name: "root".to_owned(),
            level: Some(LogLevelFilter::Error),
            appender_ref: Some(arefs),
            filter: None,
            additive: Some(false),
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use log::LogLevelFilter;
    use rustc_serialize::{Decodable, Decoder};
    use std::str::FromStr;
    use super::*;

    impl Decodable for Logger {
        fn decode<D: Decoder>(d: &mut D) -> Result<Logger, D::Error> {
            d.read_struct("Logger", 5, |d| {
                let name = try!(d.read_struct_field("name", 1, |d| Decodable::decode(d)));
                let level = try!(d.read_struct_field("level", 2, |d| Decodable::decode(d)));
                let ar = try!(d.read_struct_field("appender_ref", 3, |d| Decodable::decode(d)));
                let filter = try!(d.read_struct_field("filter", 4, |d| Decodable::decode(d)));
                let additive = try!(d.read_struct_field("additive", 5, |d| Decodable::decode(d)));

                let lvl = match level {
                    Some(s) => s,
                    None => "Error".to_owned(),
                };

                match LogLevelFilter::from_str(&lvl[..]) {
                    Ok(llf) => {
                        Ok(Logger {
                            name: name,
                            level: Some(llf),
                            appender_ref: ar,
                            filter: filter,
                            additive: additive,
                        })
                    }
                    Err(_) => Err(d.error("Error converting level field")),
                }
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::appender_ref::AppenderRefType;
    use config::filter::FilterType;
    use config::serde::LogLevelFilterField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum LoggerField {
        Name,
        Level,
        AppenderRef,
        Filter,
        Additive,
    }

    impl Deserialize for LoggerField {
        fn deserialize<D>(deserializer: &mut D) -> Result<LoggerField, D::Error>
            where D: Deserializer
        {
            struct LoggerFieldVisitor;

            impl Visitor for LoggerFieldVisitor {
                type Value = LoggerField;

                fn visit_str<E>(&mut self, value: &str) -> Result<LoggerField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "name" => Ok(LoggerField::Name),
                        "level" => Ok(LoggerField::Level),
                        "appender_ref" => Ok(LoggerField::AppenderRef),
                        "filter" => Ok(LoggerField::Filter),
                        "additive" => Ok(LoggerField::Additive),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(LoggerFieldVisitor)
        }
    }

    impl Deserialize for Logger {
        fn deserialize<D>(deserializer: &mut D) -> Result<Logger, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["name",
                                                       "level",
                                                       "appender_ref",
                                                       "filter",
                                                       "additive"];
            deserializer.visit_struct("Logger", FIELDS, LoggerVisitor)
        }
    }

    struct LoggerVisitor;

    impl Visitor for LoggerVisitor {
        type Value = Logger;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<Logger, V::Error>
            where V: MapVisitor
        {
            let mut name: Option<String> = None;
            let mut level: Option<LogLevelFilterField> = None;
            let mut appender_ref: Option<Vec<AppenderRefType>> = None;
            let mut filter: Option<FilterType> = None;
            let mut additive: Option<bool> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(LoggerField::Name) => {
                        name = Some(try!(visitor.visit_value()));
                    }
                    Some(LoggerField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(LoggerField::AppenderRef) => {
                        appender_ref = Some(try!(visitor.visit_value()));
                    }
                    Some(LoggerField::Filter) => {
                        filter = Some(try!(visitor.visit_value()));
                    }
                    Some(LoggerField::Additive) => {
                        additive = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let nm = match name {
                Some(n) => n,
                None => return visitor.missing_field("name"),
            };

            let lvl = match level {
                Some(l) => Some(l.level()),
                None => None,
            };

            try!(visitor.end());

            Ok(Logger::new()
                   .set_name(nm)
                   .set_level(lvl)
                   .set_appender_ref(appender_ref)
                   .set_filter(filter)
                   .set_additive(additive))
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use log::LogLevelFilter;
    use std::default::Default;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
name = "root"
    "#;
    const ALL_CONFIG: &'static str = r#"
name = "root"
level = "Debug"
additive = false

[filter.threshold]
level = "Info"

[[appender_ref]]
name = "console"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
    blah = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
name = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2];

    #[test]
    fn test_default() {
        let logger: Logger = Default::default();
        assert!(logger.name == "root");
        assert!(logger.level == Some(LogLevelFilter::Error));
        assert!(logger.appender_ref.is_some());
        assert!(logger.filter.is_none());
        assert!(logger.additive.is_some());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<Logger>(valid) {
                Ok(_) => results.push(true),
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<Logger>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => results.push(true),
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
