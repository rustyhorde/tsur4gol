//! The RegexFilter allows the formatted or unformatted message to be compared against a regular
//! expression.
use Filter;
use config::filter::MatchAction;
use config::filter::MatchAction::*;
use log::{LogLevelFilter, LogRecord};
use regex::Regex;

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Regex Filter Configuration
pub struct RegexFilter {
    /// Optional minimal level of messages to be filtered. Anything at or above this level will be
    /// filtered out if `regex` has been matched. The default is `Warn` meaning any messages that
    /// are lower than `Warn` (i.e. `Error`) will be logged regardless of match.
    level: Option<LogLevelFilter>,
    /// Regex
    regex: String,
    // Use the raw messge.  The default `false` indicates the formatted message will be used.
    use_raw: Option<bool>,
    /// The action to take when the filter matches.  Defaults to `Neutral`.
    on_match: Option<MatchAction>,
    /// The action to take when the filter does not match.  Defaults to `Deny`.
    on_mismatch: Option<MatchAction>,
}

impl RegexFilter {
    /// Create a new RegexFilter with the given name.
    pub fn new(regex: String) -> RegexFilter {
        RegexFilter {
            level: None,
            regex: regex,
            use_raw: None,
            on_match: None,
            on_mismatch: None,
        }
    }

    /// Set the minimum level to be filtered if the regex matches.  Anything lower will always be
    /// logged.
    pub fn level(mut self, level: Option<LogLevelFilter>) -> RegexFilter {
        self.level = level;
        self
    }

    /// Use the raw message to compare to the regex, rather than the formatted message.
    pub fn use_raw(mut self, use_raw: Option<bool>) -> RegexFilter {
        self.use_raw = use_raw;
        self
    }

    /// Set the on match MatchAction.  Default is Neutral.
    pub fn on_match(mut self, action: Option<MatchAction>) -> RegexFilter {
        self.on_match = action;
        self
    }

    /// Set the on mis-match MatchAction.  Default is Deny.
    pub fn on_mismatch(mut self, action: Option<MatchAction>) -> RegexFilter {
        self.on_mismatch = action;
        self
    }
}

impl Filter for RegexFilter {
    fn filter(&self, record: &LogRecord) -> MatchAction {
        let level = match self.level {
            Some(l) => l,
            None => LogLevelFilter::Warn,
        };

        let use_raw = match self.use_raw {
            Some(ur) => ur,
            None => false,
        };

        let matched = match Regex::new(&self.regex[..]) {
            Ok(re) => {
                if use_raw {
                    re.is_match(&format!("{}", record.args())[..])
                } else {
                    // TODO: match against formatted message when pattern is implemented.
                    true
                }
            }
            Err(_) => true,
        };

        // Check for a match, or if the record level is less that the configured minimum level.
        if matched || record.level() < level {
            // Return on_match result.
            match self.on_match {
                Some(ref m) => m.clone(),
                None => Neutral,
            }
        } else {
            // Return on_mismatch result.
            match self.on_mismatch {
                Some(ref m) => m.clone(),
                None => Deny,
            }
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use config::rs::read_llf_opt;
    use rustc_serialize::{Decodable, Decoder};
    use super::*;

    impl Decodable for RegexFilter {
        fn decode<D: Decoder>(d: &mut D) -> Result<RegexFilter, D::Error> {
            d.read_struct("RegexFilter", 5, |d| {
                let level = try!(d.read_struct_field("level", 1, |d| d.read_option(read_llf_opt)));
                let regex = try!(d.read_struct_field("regex", 2, |d| Decodable::decode(d)));
                let use_raw = try!(d.read_struct_field("use_raw", 3, |d| Decodable::decode(d)));
                let on_match = try!(d.read_struct_field("on_match", 4, |d| Decodable::decode(d)));
                let on_mismatch = try!(d.read_struct_field("on_mismatch",
                                                           5,
                                                           |d| Decodable::decode(d)));

                let rf = RegexFilter::new(regex)
                             .level(level)
                             .use_raw(use_raw)
                             .on_match(on_match)
                             .on_mismatch(on_mismatch);

                Ok(rf)
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::serde::LogLevelFilterField;
    use config::filter::serde::MatchActionField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum RegexFilterField {
        Level,
        Regex,
        UseRaw,
        OnMatch,
        OnMismatch,
    }

    impl Deserialize for RegexFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<RegexFilterField, D::Error>
            where D: Deserializer
        {
            struct RegexFilterFieldVisitor;

            impl Visitor for RegexFilterFieldVisitor {
                type Value = RegexFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<RegexFilterField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "level" => Ok(RegexFilterField::Level),
                        "regex" => Ok(RegexFilterField::Regex),
                        "use_raw" => Ok(RegexFilterField::UseRaw),
                        "on_match" => Ok(RegexFilterField::OnMatch),
                        "on_mismatch" => Ok(RegexFilterField::OnMismatch),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(RegexFilterFieldVisitor)
        }
    }

    impl Deserialize for RegexFilter {
        fn deserialize<D>(deserializer: &mut D) -> Result<RegexFilter, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["level",
                                                       "regex",
                                                       "use_raw",
                                                       "on_match",
                                                       "on_mismatch"];
            deserializer.visit_struct("RegexFilter", FIELDS, RegexFilterVisitor)
        }
    }

    struct RegexFilterVisitor;

    impl Visitor for RegexFilterVisitor {
        type Value = RegexFilter;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<RegexFilter, V::Error>
            where V: MapVisitor
        {
            let mut level: Option<LogLevelFilterField> = None;
            let mut regex: Option<String> = None;
            let mut use_raw: Option<bool> = None;
            let mut on_match: Option<MatchActionField> = None;
            let mut on_mismatch: Option<MatchActionField> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(RegexFilterField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(RegexFilterField::Regex) => {
                        regex = Some(try!(visitor.visit_value()));
                    }
                    Some(RegexFilterField::UseRaw) => {
                        use_raw = Some(try!(visitor.visit_value()));
                    }
                    Some(RegexFilterField::OnMatch) => {
                        on_match = Some(try!(visitor.visit_value()));
                    }
                    Some(RegexFilterField::OnMismatch) => {
                        on_mismatch = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let lvl = match level {
                Some(l) => Some(l.level()),
                None => None,
            };

            let re = match regex {
                Some(r) => r,
                None => return visitor.missing_field("regex"),
            };

            let omma = match on_match {
                Some(om) => Some(om.match_action()),
                None => None,
            };

            let ommma = match on_mismatch {
                Some(omm) => Some(omm.match_action()),
                None => None,
            };

            try!(visitor.end());

            let rf = RegexFilter::new(re)
                         .level(lvl)
                         .use_raw(use_raw)
                         .on_match(omma)
                         .on_mismatch(ommma);

            Ok(rf)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
regex = "^Blah"
    "#;
    const ALL_CONFIG: &'static str = r#"
regex = "^Blah"
level = "Debug"
use_raw = true
on_match = "Accept"
on_mismatch = "Neutral"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
regex = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
regex = ""
level = "NOt A LeVel"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
regex = ""
use_raw = "NOT A BOOL"
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
regex = ""
on_match = 1
    "#;
    const INVALID_CONFIG_5: &'static str = r#"
regex = ""
on_mismatch = 1
    "#;
    const INVALID_CONFIG_6: &'static str = r#"
invalid = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4,
                                                 INVALID_CONFIG_5,
                                                 INVALID_CONFIG_6];

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<RegexFilter>(valid) {
                Ok(_) => results.push(true),
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<RegexFilter>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => results.push(true),
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
