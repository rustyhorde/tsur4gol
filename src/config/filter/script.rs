//! The ScriptFilter executes a script that returns true or false.
use Filter;
use config::filter::MatchAction;
use config::filter::MatchAction::*;
use log::{LogLevelFilter, LogRecord};
use std::process::Command;

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Script Filter struct
pub struct ScriptFilter {
    /// Optional minimal level of messages to be filtered. Anything at or above this level will be
    /// filtered out if `max_burst` has been exceeded. The default is `Warn` meaning any messages
    /// that are lower than `Warn` (i.e. `Error`) will be logged regardless of the size of a burst.
    level: Option<LogLevelFilter>,
    /// Filter name
    script: String,
    /// The action to take when the filter matches.  Defaults to `Neutral`.
    on_match: Option<MatchAction>,
    /// The action to take when the filter does not match.  Defaults to `Deny`.
    on_mismatch: Option<MatchAction>,
}

impl ScriptFilter {
    /// Create a new ScriptFilter with the given script to run.
    pub fn new(script: String) -> ScriptFilter {
        ScriptFilter {
            script: script,
            level: None,
            on_match: None,
            on_mismatch: None,
        }
    }

    /// Set the minimum level to be filtered if the regex matches.  Anything lower will always be
    /// logged.
    pub fn level(mut self, level: Option<LogLevelFilter>) -> ScriptFilter {
        self.level = level;
        self
    }

    /// Set the on match MatchAction.  Default is Neutral.
    pub fn on_match(mut self, action: Option<MatchAction>) -> ScriptFilter {
        self.on_match = action;
        self
    }

    /// Set the on mis-match MatchAction.  Default is Deny.
    pub fn on_mismatch(mut self, action: Option<MatchAction>) -> ScriptFilter {
        self.on_mismatch = action;
        self
    }
}

impl Filter for ScriptFilter {
    fn filter(&self, record: &LogRecord) -> MatchAction {
        let level = match self.level {
            Some(l) => l,
            None => LogLevelFilter::Warn,
        };

        let cmdstr = format!("{} {} {} {} {} {} {}",
                             self.script,
                             record.target(),
                             record.level(),
                             record.args(),
                             record.location().module_path(),
                             record.location().file(),
                             record.location().line());

        // TODO: Build command for nix vs. windows here.
        let mut cmd = Command::new("sh");
        cmd.arg("-c").arg(&cmdstr[..]);

        let matched = match cmd.output() {
            Ok(o) => o.status.success(),
            Err(_) => true,
        };

        // Check for a match, or if the record level is less that the configured minimum level.
        if matched || record.level() < level {
            // Return on_match result.
            match self.on_match {
                Some(ref m) => m.clone(),
                None => Neutral,
            }
        } else {
            // Return on_mismatch result.
            match self.on_mismatch {
                Some(ref m) => m.clone(),
                None => Deny,
            }
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use config::rs::read_llf_opt;
    use rustc_serialize::{Decodable, Decoder};
    use super::*;

    impl Decodable for ScriptFilter {
        fn decode<D: Decoder>(d: &mut D) -> Result<ScriptFilter, D::Error> {
            d.read_struct("ScriptFilter", 4, |d| {
                let level = try!(d.read_struct_field("level", 1, |d| d.read_option(read_llf_opt)));
                let script = try!(d.read_struct_field("script", 2, |d| Decodable::decode(d)));
                let on_match = try!(d.read_struct_field("on_match", 3, |d| Decodable::decode(d)));
                let on_mismatch = try!(d.read_struct_field("on_mismatch",
                                                           4,
                                                           |d| Decodable::decode(d)));

                let rf = ScriptFilter::new(script)
                             .level(level)
                             .on_match(on_match)
                             .on_mismatch(on_mismatch);

                Ok(rf)
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::serde::LogLevelFilterField;
    use config::filter::serde::MatchActionField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum ScriptFilterField {
        Level,
        Script,
        OnMatch,
        OnMismatch,
    }

    impl Deserialize for ScriptFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<ScriptFilterField, D::Error>
            where D: Deserializer
        {
            struct ScriptFilterFieldVisitor;

            impl Visitor for ScriptFilterFieldVisitor {
                type Value = ScriptFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<ScriptFilterField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "level" => Ok(ScriptFilterField::Level),
                        "script" => Ok(ScriptFilterField::Script),
                        "on_match" => Ok(ScriptFilterField::OnMatch),
                        "on_mismatch" => Ok(ScriptFilterField::OnMismatch),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(ScriptFilterFieldVisitor)
        }
    }

    impl Deserialize for ScriptFilter {
        fn deserialize<D>(deserializer: &mut D) -> Result<ScriptFilter, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["level",
                                                       "script",
                                                       "on_match",
                                                       "on_mismatch"];
            deserializer.visit_struct("ScriptFilter", FIELDS, ScriptFilterVisitor)
        }
    }

    struct ScriptFilterVisitor;

    impl Visitor for ScriptFilterVisitor {
        type Value = ScriptFilter;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<ScriptFilter, V::Error>
            where V: MapVisitor
        {
            let mut level: Option<LogLevelFilterField> = None;
            let mut script: Option<String> = None;
            let mut on_match: Option<MatchActionField> = None;
            let mut on_mismatch: Option<MatchActionField> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(ScriptFilterField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(ScriptFilterField::Script) => {
                        script = Some(try!(visitor.visit_value()));
                    }
                    Some(ScriptFilterField::OnMatch) => {
                        on_match = Some(try!(visitor.visit_value()));
                    }
                    Some(ScriptFilterField::OnMismatch) => {
                        on_mismatch = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let lvl = match level {
                Some(l) => Some(l.level()),
                None => None,
            };

            let omma = match on_match {
                Some(om) => Some(om.match_action()),
                None => None,
            };

            let ommma = match on_mismatch {
                Some(omm) => Some(omm.match_action()),
                None => None,
            };

            let scr = match script {
                Some(s) => s,
                None => return visitor.missing_field("script"),
            };

            try!(visitor.end());

            let sf = ScriptFilter::new(scr)
                         .level(lvl)
                         .on_match(omma)
                         .on_mismatch(ommma);

            Ok(sf)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
script = "scripts/filter-test.sh"
    "#;
    const ALL_CONFIG: &'static str = r#"
script = "scripts/filter-test.sh"
level = "Debug"
on_match = "Accept"
on_mismatch = "Neutral"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
    script = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
    script = ""
    level = "NOt A LeVel"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
    regex = ""
    on_match = 1
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
    regex = ""
    on_mismatch = 1
    "#;
    const INVALID_CONFIG_5: &'static str = r#"
    invalid = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4,
                                                 INVALID_CONFIG_5];

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<ScriptFilter>(valid) {
                Ok(_) => results.push(true),
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<ScriptFilter>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => results.push(true),
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
