//! Threshold Filter
//!
//! This filter returns the `on_match` result if the level in the LogRecord is the same or more
//! specific than the configured level and the `on_mismatch` value otherwise. For example, if the
//! ThresholdFilter is configured with Level `Error` and the LogRecord contains Level `Debug` then
//! the `on_mismatch` value will be returned since `Error` events are more specific than `Debug`.
use Filter;
use config::filter::MatchAction;
use config::filter::MatchAction::*;
use log::{LogLevel, LogLevelFilter, LogMetadata, LogRecord};

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Threshold Filter
pub struct ThresholdFilter {
    /// Log Level to filter on.
    level: LogLevelFilter,
    /// Action to take on match.  Default is Neutral.
    on_match: Option<MatchAction>,
    /// Action to take on no match. Default is Deny.
    on_mismatch: Option<MatchAction>,
}

impl ThresholdFilter {
    /// Create a new ThresholdFilter with the given minimum level.
    pub fn new(level: LogLevelFilter) -> ThresholdFilter {
        ThresholdFilter {
            level: level,
            on_match: None,
            on_mismatch: None,
        }
    }

    /// Set the on match MatchAction.  Default is `Neutral`.
    pub fn on_match(mut self, action: Option<MatchAction>) -> ThresholdFilter {
        self.on_match = action;
        self
    }

    /// Set the on mis-match MatchAction.  Default is `Deny`.
    pub fn on_mismatch(mut self, action: Option<MatchAction>) -> ThresholdFilter {
        self.on_mismatch = action;
        self
    }

    fn filter(&self, level: LogLevel) -> MatchAction {
        if level <= self.level {
            // Take match action here.
            match self.on_match {
                Some(ref a) => a.clone(),
                None => Neutral,
            }
        } else {
            // Take mismatch action here.
            match self.on_mismatch {
                Some(ref a) => a.clone(),
                None => Deny,
            }
        }
    }
}

impl Filter for ThresholdFilter {
    fn filter(&self, record: &LogRecord) -> MatchAction {
        self.filter(record.level())
    }

    fn filter_by_meta(&self, meta: &LogMetadata) -> MatchAction {
        self.filter(meta.level())
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use config::rs::read_llf;
    use rustc_serialize::{Decodable, Decoder};
    use super::*;

    impl Decodable for ThresholdFilter {
        fn decode<D: Decoder>(d: &mut D) -> Result<ThresholdFilter, D::Error> {
            d.read_struct("ThresholdFilter", 3, |d| {
                let level = try!(d.read_struct_field("level", 1, read_llf));
                let on_match = try!(d.read_struct_field("on_match", 2, |d| Decodable::decode(d)));
                let on_mismatch = try!(d.read_struct_field("on_mismatch",
                                                           3,
                                                           |d| Decodable::decode(d)));
                let tf = ThresholdFilter::new(level)
                             .on_match(on_match)
                             .on_mismatch(on_mismatch);
                Ok(tf)
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::serde::LogLevelFilterField;
    use config::filter::serde::MatchActionField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum ThresholdFilterField {
        Level,
        OnMatch,
        OnMismatch,
    }

    impl Deserialize for ThresholdFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<ThresholdFilterField, D::Error>
            where D: Deserializer
        {
            struct ThresholdFilterFieldVisitor;

            impl Visitor for ThresholdFilterFieldVisitor {
                type Value = ThresholdFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<ThresholdFilterField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "level" => Ok(ThresholdFilterField::Level),
                        "on_match" => Ok(ThresholdFilterField::OnMatch),
                        "on_mismatch" => Ok(ThresholdFilterField::OnMismatch),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(ThresholdFilterFieldVisitor)
        }
    }

    impl Deserialize for ThresholdFilter {
        fn deserialize<D>(deserializer: &mut D) -> Result<ThresholdFilter, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["level", "on_match", "on_mismatch"];
            deserializer.visit_struct("ThresholdFilter", FIELDS, ThresholdFilterVisitor)
        }
    }

    struct ThresholdFilterVisitor;

    impl Visitor for ThresholdFilterVisitor {
        type Value = ThresholdFilter;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<ThresholdFilter, V::Error>
            where V: MapVisitor
        {
            let mut level: Option<LogLevelFilterField> = None;
            let mut on_match: Option<MatchActionField> = None;
            let mut on_mismatch: Option<MatchActionField> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(ThresholdFilterField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(ThresholdFilterField::OnMatch) => {
                        on_match = Some(try!(visitor.visit_value()));
                    }
                    Some(ThresholdFilterField::OnMismatch) => {
                        on_mismatch = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let lvl = match level {
                Some(l) => l.level(),
                None => return visitor.missing_field("level"),
            };

            let omma = match on_match {
                Some(om) => Some(om.match_action()),
                None => None,
            };

            let ommma = match on_mismatch {
                Some(omm) => Some(omm.match_action()),
                None => None,
            };

            try!(visitor.end());

            let tf = ThresholdFilter::new(lvl)
                         .on_match(omma)
                         .on_mismatch(ommma);

            Ok(tf)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
level = "Debug"
    "#;
    const ALL_CONFIG: &'static str = r#"
level = "Debug"
on_match = "Accept"
on_mismatch = "Neutral"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
notafield = "not a field"
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
level = "NOt A LeVel"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
level = "Debug"
on_match = 1
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
level = "Debug"
on_mismatch = 1
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4];

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<ThresholdFilter>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<ThresholdFilter>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
