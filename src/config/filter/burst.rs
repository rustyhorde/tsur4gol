//! Rate Based Filtering
//!
//! The BurstFilter provides a mechanism to control the rate at which log events are processed by
//! silently discarding events after the maximum limit has been reached.
//!
//! # Example
//!
//! ```toml
//! [filter.burst]
//! max_burst = 10.0
//! # Optional (Defaults shown)
//! level = "Warn"
//! on_match = "Accept"
//! on_mismatch = "Deny"
//! ```
use Filter;
use chrono::{DateTime, Duration, UTC};
use config::filter::MatchAction;
use config::filter::MatchAction::*;
use log::{LogLevelFilter, LogRecord};
use std::cell::Cell;
use std::ops::Add;

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Burst Filter Configuration
pub struct BurstFilter {
    /// Optional minimal level of messages to be filtered. Anything at or above this level will be
    /// filtered out if `max_burst` has been exceeded. The default is `Warn` meaning any messages
    /// that are lower than `Warn` (i.e. `Error`) will be logged regardless of the size of a burst.
    level: Option<LogLevelFilter>,
    /// The maximum number of events per second that can occur before events are filtered.
    max_burst: f64,
    /// The action to take when the filter matches.  Defaults to `Neutral`.
    on_match: Option<MatchAction>,
    /// The action to take when the filter does not match.  Defaults to `Deny`.
    on_mismatch: Option<MatchAction>,
    /// The time when we can reset the event count to 0.
    next: Cell<DateTime<UTC>>,
    /// Events received.  If `event_count` > `max_burst`, messages will be filtered.
    event_count: Cell<u32>,
}

impl BurstFilter {
    /// Create a new burst filter with the given max burst limit in Events / Second.
    pub fn new(max_burst: f64) -> BurstFilter {
        BurstFilter {
            level: None,
            max_burst: max_burst,
            on_match: None,
            on_mismatch: None,
            next: Cell::new(UTC::now().add(Duration::seconds(1))),
            event_count: Cell::new(0),
        }
    }

    /// Set the minimum level to be filtered if the max burst limit is hit.  Anything lowere will
    /// always be logged.
    pub fn level(mut self, level: Option<LogLevelFilter>) -> BurstFilter {
        self.level = level;
        self
    }

    /// Set the on match MatchAction.  Default is Neutral.
    pub fn on_match(mut self, action: Option<MatchAction>) -> BurstFilter {
        self.on_match = action;
        self
    }

    /// Set the on mis-match MatchAction.  Default is Deny.
    pub fn on_mismatch(mut self, action: Option<MatchAction>) -> BurstFilter {
        self.on_mismatch = action;
        self
    }

    /// Set the next time to reset the event count to 0.  This is usually set to now() + 1 second.
    pub fn next(self, next: DateTime<UTC>) -> BurstFilter {
        self.next.set(next);
        self
    }

    /// Set the event count.
    pub fn event_count(self, count: u32) -> BurstFilter {
        self.event_count.set(count);
        self
    }
}

impl Filter for BurstFilter {
    fn filter(&self, record: &LogRecord) -> MatchAction {
        let now = UTC::now();
        let level = match self.level {
            Some(l) => l,
            None => LogLevelFilter::Warn,
        };

        // Increment the event count.
        self.event_count.set(self.event_count.get() + 1);
        let count = self.event_count.get();

        let matched = if record.level() < level {
            true
        } else if now > self.next.get() {
            // More than 1 second has passed, reset next and event count.
            self.next.set(now.add(Duration::seconds(1)));
            self.event_count.set(0);
            true
        } else {
            // Less than 1 second has passed.  Match if max_burst is greater than the current count,
            // else don't match.
            self.max_burst > count as f64
        };

        if matched {
            match self.on_match {
                Some(ref m) => m.clone(),
                None => Neutral,
            }
        } else {
            match self.on_mismatch {
                Some(ref m) => m.clone(),
                None => Deny,
            }
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use chrono::{Duration, UTC};
    use config::rs::read_llf_opt;
    use rustc_serialize::{Decodable, Decoder};
    use super::*;
    use std::ops::Add;

    impl Decodable for BurstFilter {
        fn decode<D: Decoder>(d: &mut D) -> Result<BurstFilter, D::Error> {
            d.read_struct("BurstFilter", 4, |d| {
                let level = try!(d.read_struct_field("level", 1, |d| d.read_option(read_llf_opt)));
                let mb = try!(d.read_struct_field("max_burst", 2, |d| Decodable::decode(d)));
                let on_match = try!(d.read_struct_field("on_match", 3, |d| Decodable::decode(d)));
                let on_mismatch = try!(d.read_struct_field("on_mismatch",
                                                           4,
                                                           |d| Decodable::decode(d)));

                let bf = BurstFilter::new(mb)
                             .level(level)
                             .on_match(on_match)
                             .on_mismatch(on_mismatch)
                             .next(UTC::now().add(Duration::seconds(1)))
                             .event_count(0);

                Ok(bf)
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::serde::LogLevelFilterField;
    use config::filter::serde::MatchActionField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum BurstFilterField {
        Level,
        MaxBurst,
        OnMatch,
        OnMismatch,
    }

    impl Deserialize for BurstFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<BurstFilterField, D::Error>
            where D: Deserializer
        {
            struct BurstFilterFieldVisitor;

            impl Visitor for BurstFilterFieldVisitor {
                type Value = BurstFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<BurstFilterField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "level" => Ok(BurstFilterField::Level),
                        "max_burst" => Ok(BurstFilterField::MaxBurst),
                        "on_match" => Ok(BurstFilterField::OnMatch),
                        "on_mismatch" => Ok(BurstFilterField::OnMismatch),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(BurstFilterFieldVisitor)
        }
    }

    impl Deserialize for BurstFilter {
        fn deserialize<D>(deserializer: &mut D) -> Result<BurstFilter, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["level",
                                                       "max_burst",
                                                       "on_match",
                                                       "on_mismatch"];
            deserializer.visit_struct("BurstFilter", FIELDS, BurstFilterVisitor)
        }
    }

    struct BurstFilterVisitor;

    impl Visitor for BurstFilterVisitor {
        type Value = BurstFilter;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<BurstFilter, V::Error>
            where V: MapVisitor
        {
            let mut level: Option<LogLevelFilterField> = None;
            let mut max_burst: Option<f64> = None;
            let mut on_match: Option<MatchActionField> = None;
            let mut on_mismatch: Option<MatchActionField> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(BurstFilterField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(BurstFilterField::MaxBurst) => {
                        max_burst = Some(try!(visitor.visit_value()));
                    }
                    Some(BurstFilterField::OnMatch) => {
                        on_match = Some(try!(visitor.visit_value()));
                    }
                    Some(BurstFilterField::OnMismatch) => {
                        on_mismatch = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let mb = match max_burst {
                Some(mb) => mb,
                None => return visitor.missing_field("max_burst"),
            };

            let lvl = match level {
                Some(l) => Some(l.level()),
                None => None,
            };

            let omma = match on_match {
                Some(om) => Some(om.match_action()),
                None => None,
            };

            let ommma = match on_mismatch {
                Some(omm) => Some(omm.match_action()),
                None => None,
            };

            try!(visitor.end());

            let bf = BurstFilter::new(mb)
                         .level(lvl)
                         .on_match(omma)
                         .on_mismatch(ommma);

            Ok(bf)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
max_burst = 10.0
    "#;
    const ALL_CONFIG: &'static str = r#"
max_burst = 10.0
level = "Debug"
on_match = "Accept"
on_mismatch = "Neutral"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
max_burst = "not a float"
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
max_burst = 10.0
level = "NOt A LeVel"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
max_burst = 10.0
on_match = 1
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
max_burst = 10.0
on_mismatch = 1
    "#;
    const INVALID_CONFIG_5: &'static str = r#"
max_burst = 10.0
really = "yes really"
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4,
                                                 INVALID_CONFIG_5];

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<BurstFilter>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => {
                    assert!(false);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<BurstFilter>(invalid) {
                Ok(_) => {
                    assert!(false);
                }
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
