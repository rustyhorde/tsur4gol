//! Filter Configuration
//!
//! Filters allow log events to be evaluated to determine if or how they should be published. A
//! Filter will be called on its ```filter``` method and will return a MatchAction, which is an enum
//! that has one of 3 values - Accept, Deny or Neutral.  See the MatchAction enum docs for more
//! information on what happens when filters match or mismatch.
//!
//! Filters may be configured in one of four locations:
//!
//! 1. Context-wide Filters are configured directly in the configuration. Events that are rejected
//! by these filters will not be passed to loggers for further processing. Once an event has been
//! accepted by a Context-wide filter it will not be evaluated by any other Context-wide Filters nor
//! will the Logger's level be used to filter the event. The event will be evaluated by Logger and
//! Appender Filters however.
//! 2. Logger Filters are configured on a specified Logger. These are evaluated after the
//! Context-wide Filters and the log Level for the Logger. Events that are rejected by these filters
//! will be discarded and the event will not be passed to a parent Logger regardless of the
//! additivity setting.
//! 3. Appender Filters are used to determine if a specific Appender should handle the formatting
//! and publication of the event.
//! 4. Appender Reference Filters are used to determine if a Logger should route the event to an
//! appender.
//!
//! ## Examples
//! ```toml
//! # Context-wide Filter
//! [filter.threshold]
//! level = "Warn"
//!
//! [[appender.stdout]]
//! name = "console"
//!
//!   # Appender Filter
//!   [appender.stdout.filter.burst]
//!   max_burst = 10.0
//!
//! [[logger]]
//! name = "config::filter::burst"
//! level = "Trace"
//! additive = false
//!
//!   [[logger.appender_ref]]
//!   name = "console"
//!
//!     # Appender Reference Filter
//!     [logger.appender_ref.filter.burst]
//!     max_burst = 5.0
//!
//! # Logger Filter
//! [logger.filter.threshold]
//! level = "Debug"
//! ```
use Filter;
use self::MatchAction::*;
use std::error::Error;
use std::fmt;
use std::str::FromStr;

pub mod burst;
pub mod regex;
pub mod script;
pub mod threshold;
pub mod time;

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Result returned by the Filter trait.
///
/// In general, a filter will return `Neutral` on a match, and `Deny` on a mismatch.  `Neutral`
/// signifies that the current filter matched, but any subsequent filters should also be checked.
/// `Accept` signifies that the filter matched, and the event should be passed on, skipping any
/// subsequent filter checks.  `Deny` means the filter failed and the event should be discarded.
pub enum MatchAction {
    /// The filter accepts the log record.  No other filters should be checked.
    Accept,
    /// The filter denies.  The log event should be discarded.
    Deny,
    /// The filter is neutral, filter checks continue.
    Neutral,
}

#[derive(Debug)]
/// Error thrown when parsing incorrect configuration TOML for filters.
///
/// This error can be thrown when parsing configuration TOML for onmatch and onmismatch values if
/// they are not one of Accept, accept, Neutral, neutral, Deny, or deny.
pub struct FromStrError {
    /// The error description.
    desc: String,
    /// The error detail.
    detail: String,
}

impl fmt::Display for FromStrError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: {}", self.desc, self.detail)
    }
}

impl Error for FromStrError {
    fn description(&self) -> &str {
        &self.desc[..]
    }
}

impl FromStr for MatchAction {
    type Err = FromStrError;
    fn from_str(s: &str) -> Result<MatchAction, FromStrError> {
        match s {
            "Accept" | "accept" => Ok(Accept),
            "Deny" | "deny" => Ok(Deny),
            "Neutral" | "neutral" => Ok(Neutral),
            _ => {
                Err(FromStrError {
                    desc: "Invalid MatchAction".to_owned(),
                    detail: s.to_owned(),
                })
            }
        }
    }
}

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug, Default)]
/// Contains one or more filter configurations.
///
/// For each FilterType, you may specify one or more filters.  Each specific filter can only be
/// specified once per FilteType however (i.e You can't configure two BurstFilters in one
/// FilterType).
pub struct FilterType {
    /// Rate Based Filter
    burst: Option<burst::BurstFilter>,
    /// Regex Based Filter
    regex: Option<regex::RegexFilter>,
    /// Script Based Filter
    script: Option<script::ScriptFilter>,
    /// LogLevelFilter Based Filter
    threshold: Option<threshold::ThresholdFilter>,
    /// Time Based Filter
    time: Option<time::TimeFilter>,
}

impl FilterType {
    /// Create a new empty FilterType.
    pub fn new() -> FilterType {
        Default::default()
    }

    /// Add a BurstFilter to the FilterType.
    pub fn burst(mut self, burst: Option<burst::BurstFilter>) -> FilterType {
        self.burst = burst;
        self
    }

    /// Add a RegexFilter to the FilterType.
    pub fn regex(mut self, regex: Option<regex::RegexFilter>) -> FilterType {
        self.regex = regex;
        self
    }

    /// Add a ScriptFilter to the FilterType.
    pub fn script(mut self, script: Option<script::ScriptFilter>) -> FilterType {
        self.script = script;
        self
    }

    /// Add a ThresholdFilter to the FilterType.
    pub fn threshold(mut self, threshold: Option<threshold::ThresholdFilter>) -> FilterType {
        self.threshold = threshold;
        self
    }

    /// Add a TimeFilter to the FilterType.
    pub fn time(mut self, time: Option<time::TimeFilter>) -> FilterType {
        self.time = time;
        self
    }
}

impl Into<Vec<Box<Filter>>> for FilterType {
    fn into(self) -> Vec<Box<Filter>> {
        let mut boxed: Vec<Box<Filter>> = Vec::new();

        if let Some(f) = self.burst {
            boxed.push(Box::new(f));
        }

        if let Some(f) = self.regex {
            boxed.push(Box::new(f));
        }

        if let Some(f) = self.script {
            boxed.push(Box::new(f));
        }

        if let Some(f) = self.threshold {
            boxed.push(Box::new(f));
        }

        if let Some(f) = self.time {
            boxed.push(Box::new(f));
        }

        boxed
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use rustc_serialize::{Decodable, Decoder};
    use std::error::Error;
    use std::str::FromStr;
    use super::*;

    impl Decodable for MatchAction {
        fn decode<D: Decoder>(d: &mut D) -> Result<MatchAction, D::Error> {
            let match_str = try!(d.read_str());
            match MatchAction::from_str(&match_str[..]) {
                Ok(m) => Ok(m),
                Err(e) => Err(d.error(e.description())),
            }
        }
    }

    impl Decodable for FilterType {
        fn decode<D: Decoder>(d: &mut D) -> Result<FilterType, D::Error> {
            d.read_struct("FilteType", 5, |d| {
                Ok(FilterType {
                    burst: try!(d.read_struct_field("burst", 1, |d| Decodable::decode(d))),
                    regex: try!(d.read_struct_field("regex", 2, |d| Decodable::decode(d))),
                    script: try!(d.read_struct_field("script", 3, |d| Decodable::decode(d))),
                    threshold: try!(d.read_struct_field("threshold", 4, |d| Decodable::decode(d))),
                    time: try!(d.read_struct_field("time", 5, |d| Decodable::decode(d))),
                })
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    pub struct MatchActionField {
        match_action: MatchAction,
    }

    impl Default for MatchActionField {
        fn default() -> MatchActionField {
            MatchActionField { match_action: MatchAction::Deny }
        }
    }

    impl MatchActionField {
        pub fn match_action(&self) -> MatchAction {
            self.match_action.clone()
        }
    }

    impl Deserialize for MatchActionField {
        fn deserialize<D>(deserializer: &mut D) -> Result<MatchActionField, D::Error>
            where D: Deserializer
        {
            struct MatchActionFieldVisitor;

            impl Visitor for MatchActionFieldVisitor {
                type Value = MatchActionField;

                fn visit_str<E>(&mut self, value: &str) -> Result<MatchActionField, E>
                    where E: ::serde::de::Error
                {
                    let mut maf: MatchActionField = Default::default();

                    match value {
                        "Accept" | "accept" => maf.match_action = MatchAction::Accept,
                        "Neutral" | "neutral" => maf.match_action = MatchAction::Neutral,
                        "Deny" | "deny" => maf.match_action = MatchAction::Deny,
                        _ => {
                            return Err(::serde::de::Error::syntax("Unexpected MatchAction value!"))
                        }
                    }

                    Ok(maf)
                }
            }

            deserializer.visit(MatchActionFieldVisitor)
        }
    }

    enum FilterTypeField {
        Burst,
        Regex,
        Script,
        Threshold,
        Time,
    }

    impl Deserialize for FilterTypeField {
        fn deserialize<D>(deserializer: &mut D) -> Result<FilterTypeField, D::Error>
            where D: Deserializer
        {
            struct FilterTypeFieldVisitor;

            impl Visitor for FilterTypeFieldVisitor {
                type Value = FilterTypeField;

                fn visit_str<E>(&mut self, value: &str) -> Result<FilterTypeField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "burst" => Ok(FilterTypeField::Burst),
                        "regex" => Ok(FilterTypeField::Regex),
                        "script" => Ok(FilterTypeField::Script),
                        "threshold" => Ok(FilterTypeField::Threshold),
                        "time" => Ok(FilterTypeField::Time),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(FilterTypeFieldVisitor)
        }
    }

    impl Deserialize for FilterType {
        fn deserialize<D>(deserializer: &mut D) -> Result<FilterType, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["burst",
                                                       "regex",
                                                       "script",
                                                       "threshold",
                                                       "time"];
            deserializer.visit_struct("FilterType", FIELDS, FilterTypeVisitor)
        }
    }

    struct FilterTypeVisitor;

    impl Visitor for FilterTypeVisitor {
        type Value = FilterType;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<FilterType, V::Error>
            where V: MapVisitor
        {
            let mut burst: Option<burst::BurstFilter> = None;
            let mut regex: Option<regex::RegexFilter> = None;
            let mut script: Option<script::ScriptFilter> = None;
            let mut threshold: Option<threshold::ThresholdFilter> = None;
            let mut time: Option<time::TimeFilter> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(FilterTypeField::Burst) => {
                        burst = Some(try!(visitor.visit_value()));
                    }
                    Some(FilterTypeField::Regex) => {
                        regex = Some(try!(visitor.visit_value()));
                    }
                    Some(FilterTypeField::Script) => {
                        script = Some(try!(visitor.visit_value()));
                    }
                    Some(FilterTypeField::Threshold) => {
                        threshold = Some(try!(visitor.visit_value()));
                    }
                    Some(FilterTypeField::Time) => {
                        time = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            try!(visitor.end());

            let ft = FilterType::new()
                         .burst(burst)
                         .regex(regex)
                         .script(script)
                         .threshold(threshold)
                         .time(time);

            Ok(ft)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use std::default::Default;
    use std::str::FromStr;
    use super::*;

    #[test]
    fn test_match_action() {
        assert!(MatchAction::from_str("accept").expect("Failed!") == MatchAction::Accept);
        assert!(MatchAction::from_str("Accept").expect("Failed!") == MatchAction::Accept);
        assert!(MatchAction::from_str("deny").expect("Failed!") == MatchAction::Deny);
        assert!(MatchAction::from_str("Deny").expect("Failed!") == MatchAction::Deny);
        assert!(MatchAction::from_str("neutral").expect("Failed!") == MatchAction::Neutral);
        assert!(MatchAction::from_str("Neutral").expect("Failed!") == MatchAction::Neutral);
        match MatchAction::from_str("AccEPt") {
            Ok(_) => assert!(false),
            Err(e) => {
                println!("{:?}", e);
                assert!(true);
            }
        }
    }

    const BASE_CONFIG: &'static str = r#""#;
    const ALL_CONFIG: &'static str = r#"
[burst]
max_burst = 10.0

[regex]
regex = "^\\d&"

[script]
script = "a_script.sh"

[threshold]
level = "Info"

[time]
start = "20:00:00"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#"
    blah = 1
    "#;
    const INVALID_CONFIG_1: &'static str = r#"
[notafilter]
a = 1
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
[threshold]
level = "Info"
on_match = "Woot"
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2];

    #[test]
    fn test_default() {
        let ft: FilterType = Default::default();
        assert!(ft.burst.is_none());
        assert!(ft.regex.is_none());
        assert!(ft.script.is_none());
        assert!(ft.threshold.is_none());
        assert!(ft.time.is_none());
    }

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<FilterType>(valid) {
                Ok(_) => {
                    results.push(true);
                }
                Err(_) => {
                    assert!(false);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<FilterType>(invalid) {
                Ok(_) => {
                    assert!(false);
                }
                Err(_) => {
                    results.push(true);
                }
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
