//! Time Filter
//!
//! The time filter can be used to restrict log events to only a certain portion of the day.
//!
//! # Example
//!
//! ```toml
//! # Restrict log events between 22:00:00 UTC and 23:59:59 UTC to only Error.
//! [filter.time]
//! start = 22:00:00
//! # Optional (Defaults shown)
//! end = 23:59:59
//! level = "Warn"
//! offset = 0
//! exclude = true
//! on_match = "Accept"
//! on_mismatch = "Deny"
//! ```
use Filter;
use chrono::{Duration, NaiveTime, UTC};
use config::filter::MatchAction;
use config::filter::MatchAction::*;
use log::{LogLevelFilter, LogRecord};

#[cfg_attr(test, derive(PartialEq))]
#[derive(Clone, Debug)]
/// Time Filter struct
pub struct TimeFilter {
    /// Optional level of messages to be filtered. Anything at or below this level will be filtered
    /// out if within the specified time. The default is `Warn` meaning any messages that are
    /// higher than `Warn` will be logged regardless of the time.
    level: Option<LogLevelFilter>,
    /// A time in HH:mm:ss format.
    start: NaiveTime,
    /// A time in HH:mm:ss format.  Specifying an end time less than the start time will result in
    /// no log entries being written.  If no end time is specified, defaults to 23:59:59.
    end: Option<NaiveTime>,
    /// The timezone offset from UTC to use when comparing `start` and `end` to the event timestamp.
    /// The default is 0 meaning all times are compared in UTC.
    offset: Option<i64>,
    /// Flips the check.  If false, if the event falls between start and end it will be included,
    /// rather than exclude.  The default is true.
    exclude: Option<bool>,
    /// Action to take when the filter matches. May be `Accept`, `Deny` or `Neutral`. The default
    /// value is `Neutral`.
    on_match: Option<MatchAction>,
    /// Action to take when the filter does not match. May be `Accept`, `Deny` or `Neutral`. The
    /// default value is `Deny`.
    on_mismatch: Option<MatchAction>,
}

impl TimeFilter {
    /// Create a new ThresholdFilter with the given minimum level.
    pub fn new(start: NaiveTime) -> TimeFilter {
        TimeFilter {
            level: None,
            start: start,
            end: None,
            offset: None,
            exclude: None,
            on_match: None,
            on_mismatch: None,
        }
    }

    /// Set the minimum level to be filtered.
    pub fn level(mut self, level: Option<LogLevelFilter>) -> TimeFilter {
        self.level = level;
        self
    }

    /// Set the end NaiveTime to filter.
    pub fn end(mut self, end: Option<NaiveTime>) -> TimeFilter {
        self.end = end;
        self
    }

    /// Set the timezone offset from UTC.
    pub fn offset(mut self, offset: Option<i64>) -> TimeFilter {
        self.offset = offset;
        self
    }

    /// Set the timezone offset from UTC.
    pub fn exclude(mut self, exclude: Option<bool>) -> TimeFilter {
        self.exclude = exclude;
        self
    }

    /// Set the on match MatchAction.  Default is `Neutral`.
    pub fn on_match(mut self, action: Option<MatchAction>) -> TimeFilter {
        self.on_match = action;
        self
    }

    /// Set the on mis-match MatchAction.  Default is `Deny`.
    pub fn on_mismatch(mut self, action: Option<MatchAction>) -> TimeFilter {
        self.on_mismatch = action;
        self
    }
}

impl Filter for TimeFilter {
    fn filter(&self, record: &LogRecord) -> MatchAction {
        // now as UTC
        let now = UTC::now();

        // Get the offset from UTC from the config.
        let offset = match self.offset {
            Some(o) => o,
            None => 0,
        };

        // Get the exclude flag.  Defaults to true.  Exclude false means include when between start
        // and end.
        let exclude = match self.exclude {
            Some(e) => e,
            None => true,
        };

        // Get the minimum level to filter.
        let level = match self.level {
            Some(l) => l,
            None => LogLevelFilter::Warn,
        };

        // Setup the NaiveTime for the end parameter.
        let naive_end = match self.end {
            Some(e) => e,
            None => NaiveTime::from_hms(23, 59, 59),
        };

        // The start datetime with offset.
        let start_dt = match now.date().and_time(self.start) {
            Some(s) => s,
            None => now,
        };

        // The end datetime with offset.
        let end_dt = match now.date().and_time(naive_end) {
            Some(e) => e,
            None => now,
        };

        // The start datetime in UTC.
        let start_dt_utc = start_dt - Duration::hours(offset);
        // The end datetime in UTC.
        let end_dt_utc = end_dt - Duration::hours(offset);

        // now is not between start and end
        let mut matched = !((start_dt_utc <= now) && (now <= end_dt_utc));

        // If we are including, rather than excluding, flip the match.
        if !exclude {
            matched = !matched
        }

        // Check for a match, or if the record level is less that the configured minimum level.
        if matched || record.level() < level {
            // Return on_match result.
            match self.on_match {
                Some(ref m) => m.clone(),
                None => Neutral,
            }
        } else {
            // Return on_mismatch result.
            match self.on_mismatch {
                Some(ref m) => m.clone(),
                None => Deny,
            }
        }
    }
}

#[cfg(feature = "rustc-serialize")]
mod rs {
    use config::rs::{read_llf_opt, read_naivetime, read_naivetime_opt};
    use rustc_serialize::{Decodable, Decoder};
    use super::*;

    impl Decodable for TimeFilter {
        fn decode<D: Decoder>(d: &mut D) -> Result<TimeFilter, D::Error> {
            d.read_struct("TimeFilter", 6, |d| {
                let level = try!(d.read_struct_field("level", 1, |d| d.read_option(read_llf_opt)));
                let start = try!(d.read_struct_field("start", 2, read_naivetime));
                let end = try!(d.read_struct_field("end",
                                                   3,
                                                   |d| d.read_option(read_naivetime_opt)));
                let offset = try!(d.read_struct_field("offset", 4, |d| Decodable::decode(d)));
                let exclude = try!(d.read_struct_field("exclude", 5, |d| Decodable::decode(d)));
                let on_match = try!(d.read_struct_field("on_match", 6, |d| Decodable::decode(d)));
                let on_mismatch = try!(d.read_struct_field("on_mismatch",
                                                           7,
                                                           |d| Decodable::decode(d)));

                let tf = TimeFilter::new(start)
                             .level(level)
                             .end(end)
                             .offset(offset)
                             .exclude(exclude)
                             .on_match(on_match)
                             .on_mismatch(on_mismatch);

                Ok(tf)
            })
        }
    }
}

#[cfg(feature = "serde")]
mod serde {
    use config::serde::{LogLevelFilterField, NaiveTimeField};
    use config::filter::serde::MatchActionField;
    use super::*;
    use serde::{Deserialize, Deserializer};
    use serde::de::{MapVisitor, Visitor};

    enum TimeFilterField {
        Level,
        Start,
        End,
        Offset,
        Exclude,
        OnMatch,
        OnMismatch,
    }

    impl Deserialize for TimeFilterField {
        fn deserialize<D>(deserializer: &mut D) -> Result<TimeFilterField, D::Error>
            where D: Deserializer
        {
            struct TimeFilterFieldVisitor;

            impl Visitor for TimeFilterFieldVisitor {
                type Value = TimeFilterField;

                fn visit_str<E>(&mut self, value: &str) -> Result<TimeFilterField, E>
                    where E: ::serde::de::Error
                {
                    match value {
                        "level" => Ok(TimeFilterField::Level),
                        "start" => Ok(TimeFilterField::Start),
                        "end" => Ok(TimeFilterField::End),
                        "offset" => Ok(TimeFilterField::Offset),
                        "exclude" => Ok(TimeFilterField::Exclude),
                        "on_match" => Ok(TimeFilterField::OnMatch),
                        "on_mismatch" => Ok(TimeFilterField::OnMismatch),
                        _ => Err(::serde::de::Error::syntax("Unexpected field!")),
                    }
                }
            }

            deserializer.visit(TimeFilterFieldVisitor)
        }
    }

    impl Deserialize for TimeFilter {
        fn deserialize<D>(deserializer: &mut D) -> Result<TimeFilter, D::Error>
            where D: Deserializer
        {
            static FIELDS: &'static [&'static str] = &["level",
                                                       "start",
                                                       "end",
                                                       "offset",
                                                       "exclude",
                                                       "on_match",
                                                       "on_mismatch"];
            deserializer.visit_struct("TimeFilter", FIELDS, TimeFilterVisitor)
        }
    }

    struct TimeFilterVisitor;

    impl Visitor for TimeFilterVisitor {
        type Value = TimeFilter;

        fn visit_map<V>(&mut self, mut visitor: V) -> Result<TimeFilter, V::Error>
            where V: MapVisitor
        {
            let mut level: Option<LogLevelFilterField> = None;
            let mut start: Option<NaiveTimeField> = None;
            let mut end: Option<NaiveTimeField> = None;
            let mut offset = None;
            let mut exclude = None;
            let mut on_match: Option<MatchActionField> = None;
            let mut on_mismatch: Option<MatchActionField> = None;

            loop {
                match try!(visitor.visit_key()) {
                    Some(TimeFilterField::Level) => {
                        level = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::Start) => {
                        start = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::End) => {
                        end = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::Offset) => {
                        offset = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::Exclude) => {
                        exclude = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::OnMatch) => {
                        on_match = Some(try!(visitor.visit_value()));
                    }
                    Some(TimeFilterField::OnMismatch) => {
                        on_mismatch = Some(try!(visitor.visit_value()));
                    }
                    None => {
                        break;
                    }
                }
            }

            let start_nt = match start {
                Some(s) => s.naive_time(),
                None => return visitor.missing_field("start"),
            };

            let lvl = match level {
                Some(l) => Some(l.level()),
                None => None,
            };

            let end_nt = match end {
                Some(e) => Some(e.naive_time()),
                None => None,
            };

            let omma = match on_match {
                Some(om) => Some(om.match_action()),
                None => None,
            };

            let ommma = match on_mismatch {
                Some(omm) => Some(omm.match_action()),
                None => None,
            };

            try!(visitor.end());

            let tf = TimeFilter::new(start_nt)
                         .level(lvl)
                         .end(end_nt)
                         .offset(offset)
                         .exclude(exclude)
                         .on_match(omma)
                         .on_mismatch(ommma);

            Ok(tf)
        }
    }
}

#[cfg(test)]
mod test {
    use decode;
    use super::*;

    const BASE_CONFIG: &'static str = r#"
start = "07:00:00"
    "#;
    const ALL_CONFIG: &'static str = r#"
start = "07:00:00"
end = "20:00:00"
offset = -5
exclude = false
level = "Debug"
on_match = "Accept"
on_mismatch = "Neutral"
    "#;

    static VALIDS: &'static [&'static str] = &[BASE_CONFIG, ALL_CONFIG];

    const INVALID_CONFIG_0: &'static str = r#""#;
    const INVALID_CONFIG_1: &'static str = r#"
start = "NOt A HH:MM:SS"
    "#;
    const INVALID_CONFIG_2: &'static str = r#"
start = "07:00:00"
end = "NOt A HH:MM:SS"
    "#;
    const INVALID_CONFIG_3: &'static str = r#"
start = "07:00:00"
offset = "not a number"
    "#;
    const INVALID_CONFIG_4: &'static str = r#"
start = "07:00:00"
exclude = "not a bool"
    "#;
    const INVALID_CONFIG_5: &'static str = r#"
start = "07:00:00"
on_match = 1
    "#;
    const INVALID_CONFIG_6: &'static str = r#"
start = "07:00:00"
on_mismatch = 1
    "#;
    const INVALID_CONFIG_7: &'static str = r#"
invalid = "invalid field"
    "#;

    static INVALIDS: &'static [&'static str] = &[INVALID_CONFIG_0,
                                                 INVALID_CONFIG_1,
                                                 INVALID_CONFIG_2,
                                                 INVALID_CONFIG_3,
                                                 INVALID_CONFIG_4,
                                                 INVALID_CONFIG_5,
                                                 INVALID_CONFIG_6,
                                                 INVALID_CONFIG_7];

    #[test]
    fn test_valid_configs() {
        let mut results = Vec::new();

        for valid in VALIDS {
            match decode::<TimeFilter>(valid) {
                Ok(_) => results.push(true),
                Err(_) => assert!(false),
            };
        }

        assert!(results.iter().all(|x| *x));
    }

    #[test]
    fn test_invalid_configs() {
        let mut results = Vec::new();

        for invalid in INVALIDS {
            match decode::<TimeFilter>(invalid) {
                Ok(_) => assert!(false),
                Err(_) => results.push(true),
            };
        }

        assert!(results.iter().all(|x| *x));
    }
}
