//! Generate version information for the tsur4gol library.
include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[cfg(unix)]
fn verbose_ver() -> String {
    format!("\x1b[32;1mtsur4gol {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
             {}\nbuild-date: {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(windows)]
fn verbose_ver() -> String {
    format!("tsur4gol {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: \
             {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(unix)]
fn ver() -> String {
    format!("\x1b[32;1mtsur4gol {}\x1b[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

#[cfg(windows)]
fn ver() -> String {
    format!("tsur4gol {}[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

/// Generate a version string.
///
/// # Examples
///
/// ```
/// use tsur4gol::version;
///
/// // Normal
/// println!("{}", version::version(false));
/// // tsur4gol v0.1.3-pre-11-gd90443d (d90443d 2015-12-07) (built 2015-12-07)
///
/// // Verbose
/// println!("{}", version::version(true));
/// // tsur4gol v0.1.3-pre-11-gd90443d (d90443d 2015-12-07) (built 2015-12-07)
/// // commit-hash: d90443d92db3826c648817e6bd6cb757729f7209
/// // commit-date: 2015-12-07
/// // build-date: 2015-12-07
/// // host: x86_64-unknown-linux-gnu
/// // release: v0.1.3-pre-11-gd90443d
/// ```
pub fn version(verbose: bool) -> String {
    if verbose {
        verbose_ver()
    } else {
        ver()
    }
}
