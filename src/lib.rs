// Copyright (c) 2016 vergen developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Logging for Rust
//!
//!     //  ########  ######  ##     ## ########  ##         ######    #######  ##
//!     //     ##    ##    ## ##     ## ##     ## ##    ##  ##    ##  ##     ## ##
//!     //     ##    ##       ##     ## ##     ## ##    ##  ##        ##     ## ##
//!     //     ##     ######  ##     ## ########  ##    ##  ##   #### ##     ## ##
//!     //     ##          ## ##     ## ##   ##   ######### ##    ##  ##     ## ##
//!     //     ##    ##    ## ##     ## ##    ##        ##  ##    ##  ##     ## ##
//!     //     ##     ######   #######  ##     ##       ##   ######    #######  ########
//!
#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![cfg_attr(feature = "clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
extern crate chrono;
#[macro_use]
extern crate log;
#[cfg(feature = "mysql")]
extern crate mysql;
extern crate regex;
#[cfg(feature = "rustc-serialize")]
extern crate rustc_serialize;
#[cfg(feature = "serde")]
extern crate serde;
extern crate toml;
extern crate time;

use config::Config;
#[cfg(test)]
use config::ConfigError;
#[cfg(test)]
use config::ConfigError::ParserError;
use config::filter::MatchAction;
use log::{LogMetadata, LogRecord};
use logger::Logger;
use reloader::Reloader;
#[cfg(all(test, feature = "rustc-serialize"))]
use rustc_serialize::Decodable;
#[cfg(all(test, feature = "serde"))]
use serde::Deserialize;
use std::error::Error;
use std::fmt;
use std::io::{self, Write};
use std::path::Path;
pub use version::version;

pub mod config;
mod logger;
pub mod reloader;
pub mod version;

/// Trait for getting name of something.
pub trait Named: Send + 'static {
    /// Get the name from the Named.
    fn name(&self) -> String;
}

/// A trait implemented by log4rs appenders.
pub trait Append: Named + Send + 'static {
    /// Processes the provided `LogRecord`.
    fn append(&mut self, record: &LogRecord) -> Result<(), Box<Error>>;
}

/// The trait implemented by log4rs filters.
pub trait Filter: Send + 'static {
    /// Filters a log event.
    fn filter(&self, record: &LogRecord) -> MatchAction;

    /// Filter a log event based on LogMetadata.
    fn filter_by_meta(&self, _meta: &LogMetadata) -> MatchAction {
        MatchAction::Neutral
    }
}

/// Initializes the global logger with a tsur4gol logger configured by `config`.
pub fn init_from_toml<P>(toml: String, path: Option<P>) -> Result<Option<Reloader>, TsurgolError>
    where P: AsRef<Path>
{
    let config = try!(Config::new(&toml[..]));
    let refresh_rate = config.get_refresh_rate();
    let logger = Logger::new(config);

    let result = match (refresh_rate, path) {
        (Some(rr), Some(p)) => {
            let mut reloader = Reloader::new(&logger);
            reloader.path(p.as_ref().to_path_buf());
            reloader.rate(rr);
            Some(reloader)
        }
        (None, Some(p)) => {
            let mut reloader = Reloader::new(&logger);
            reloader.path(p.as_ref().to_path_buf());
            Some(reloader)
        }
        _ => None,
    };

    try!(log::set_logger(|max_log_level| {
        max_log_level.set(log::LogLevelFilter::Trace);
        Box::new(logger)
    }));

    Ok(result)
}

/// Initializes the global logger from a TOML configuration file.
pub fn init_from_file<P>(path: P) -> Result<Option<Reloader>, TsurgolError>
    where P: AsRef<Path>
{
    let source = try!(config::read_config(path.as_ref()));
    init_from_toml(source, Some(path))
}

fn handle_error<E: Error + ?Sized>(e: &E) -> io::Result<()> {
    let stderr = io::stderr();
    let mut stderr_lck = stderr.lock();
    writeln!(&mut stderr_lck, "{}", e)
}

#[derive(Debug)]
/// Tsur4gol Error
pub struct TsurgolError {
    desc: String,
    detail: String,
    cause: Option<Box<Error>>,
}

impl fmt::Display for TsurgolError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}: {}", self.desc, self.detail)
    }
}

impl Error for TsurgolError {
    fn description(&self) -> &str {
        &self.desc[..]
    }

    fn cause(&self) -> Option<&Error> {
        match self.cause {
            Some(ref e) => Some(&**e),
            None => None,
        }
    }
}

impl From<config::ConfigError> for TsurgolError {
    fn from(e: config::ConfigError) -> TsurgolError {
        TsurgolError {
            desc: "TsurgolError".to_owned(),
            detail: e.description().to_owned(),
            cause: Some(Box::new(e)),
        }
    }
}

impl From<log::SetLoggerError> for TsurgolError {
    fn from(e: log::SetLoggerError) -> TsurgolError {
        TsurgolError {
            desc: "TsurgolError".to_owned(),
            detail: e.description().to_owned(),
            cause: Some(Box::new(e)),
        }
    }
}

impl From<io::Error> for TsurgolError {
    fn from(e: io::Error) -> TsurgolError {
        TsurgolError {
            desc: "TsurgolError".to_owned(),
            detail: e.description().to_owned(),
            cause: Some(Box::new(e)),
        }
    }
}

#[cfg(all(test, feature = "rustc-serialize"))]
pub fn decode<T: Decodable>(tomlstr: &str) -> Result<T, ConfigError> {
    let mut parser = toml::Parser::new(tomlstr);
    match parser.parse() {
        Some(toml) => {
            let mut decoder = toml::Decoder::new(toml::Value::Table(toml));
            Ok(try!(T::decode(&mut decoder)))
        }
        None => Err(ParserError(parser.errors)),
    }
}

#[cfg(all(test, feature = "serde"))]
pub fn decode<T: Deserialize>(tomlstr: &str) -> Result<T, ConfigError> {
    let mut parser = toml::Parser::new(tomlstr);
    match parser.parse() {
        Some(toml) => {
            let mut decoder = toml::Decoder::new(toml::Value::Table(toml));
            Ok(try!(T::deserialize(&mut decoder)))
        }
        None => Err(ParserError(parser.errors)),
    }
}

#[cfg(test)]
mod test {
    use config::Config;
    use log::LogLevel::Info;
    use reloader::Reloader;
    use std::env;
    use std::fs;
    use std::path::Path;
    use std::thread;
    use std::time;
    use super::*;

    fn logi(i: usize) {
        if i < 100 {
            trace!("{}", i);
        } else if i >= 100 && i < 200 {
            debug!("{}", i);
        } else if i >= 980 && i < 990 {
            warn!("{}", i);
        } else if i >= 990 {
            error!("{}", i);
        } else {
            info!("{}", i);
        }
    }

    fn log1000() {
        thread::sleep(time::Duration::new(1, 0));

        for i in 0..1000 {
            logi(i);
        }
    }

    fn logall() {
        trace!("TRACE");
        debug!("DEBUG");
        info!("INFO");
        warn!("WARN");
        error!("ERROR");

        let res = log_enabled!(Info);
        if res {
            info!("INFO ENABLED");
        }
    }

    #[test]
    fn test_version() {
        assert!(!version(true).is_empty());
        assert!(!version(false).is_empty());
    }

    #[test]
    fn test_handle_error() {
        use config::ConfigError;

        assert!(super::handle_error(&ConfigError::ParserError(Vec::new())).is_ok());
    }

    #[test]
    fn test_parser_error() {
        assert!(decode::<Config>(r#"
[a]
b = 1

[a]
c = 2
        "#)
                    .is_err());
    }

    fn test_toml<P>(from: &str, to: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        assert!(fs::copy(Path::new(from), &to).is_ok());
        assert!(reloader.reload().is_ok());
        logall();
    }

    fn test_toml_1<P>(from: &str, to: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        assert!(fs::copy(Path::new(from), &to).is_ok());
        assert!(reloader.reload().is_ok());
        log1000();
    }

    fn test_levels<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/warn.toml", &test_path, reloader);
        test_toml("test_toml/info.toml", &test_path, reloader);
        test_toml("test_toml/debug.toml", &test_path, reloader);
        test_toml("test_toml/trace.toml", &test_path, reloader);
    }

    fn test_context_level_threshold_filter<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/context_level/threshold_00.toml",
                  &test_path,
                  reloader);
        test_toml("test_toml/context_level/threshold_01.toml",
                  &test_path,
                  reloader);
        test_toml("test_toml/context_level/threshold_02.toml",
                  &test_path,
                  reloader);
    }

    fn test_context_level_burst_filter<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml_1("test_toml/context_level/burst_00.toml",
                    &test_path,
                    reloader);
        test_toml_1("test_toml/context_level/burst_01.toml",
                    &test_path,
                    reloader);
        test_toml_1("test_toml/context_level/burst_02.toml",
                    &test_path,
                    reloader);
    }

    fn test_context_level_time_filter<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/context_level/time_00.toml", &test_path, reloader);
        test_toml("test_toml/context_level/time_01.toml", &test_path, reloader);
        test_toml("test_toml/context_level/time_02.toml", &test_path, reloader);
    }

    fn test_context_level_regex_filter<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml_1("test_toml/context_level/regex_00.toml",
                    &test_path,
                    reloader);
        test_toml_1("test_toml/context_level/regex_01.toml",
                    &test_path,
                    reloader);
    }

    fn test_context_level_script_filter<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml_1("test_toml/context_level/script_00.toml",
                    &test_path,
                    reloader);
    }

    fn test_appender_noop<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml_1("test_toml/appender/noop_00.toml", &test_path, reloader);
    }

    fn test_appender_stdout<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/appender/stdout_00.toml", &test_path, reloader);
        test_toml("test_toml/appender/stdout_01.toml", &test_path, reloader);
    }

    fn test_appender_stderr<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/appender/stderr_00.toml", &test_path, reloader);
        test_toml("test_toml/appender/stderr_01.toml", &test_path, reloader);
    }

    fn test_appender_file<P>(test_path: P, reloader: &mut Reloader)
        where P: AsRef<Path>
    {
        test_toml("test_toml/appender/file_00.toml", &test_path, reloader);
        test_toml("test_toml/appender/file_01.toml", &test_path, reloader);
    }

    #[test]
    fn test_init_config() {
        let test_path = env::temp_dir().join("test.toml");
        assert!(fs::copy(Path::new("test_toml/empty.toml"), &test_path).is_ok());
        let mut reloader = match init_from_file(&test_path) {
            Ok(Some(r)) => r,
            _ => return assert!(false),
        };
        logall();

        test_levels(&test_path, &mut reloader);
        test_context_level_threshold_filter(&test_path, &mut reloader);
        test_context_level_burst_filter(&test_path, &mut reloader);
        test_context_level_time_filter(&test_path, &mut reloader);
        test_context_level_regex_filter(&test_path, &mut reloader);
        test_context_level_script_filter(&test_path, &mut reloader);
        test_appender_noop(&test_path, &mut reloader);
        test_appender_stdout(&test_path, &mut reloader);
        test_appender_stderr(&test_path, &mut reloader);
        test_appender_file(&test_path, &mut reloader);
    }
}
