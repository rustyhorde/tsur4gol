//! Logger
use {Append, Filter, Named};
use config::filter::MatchAction::*;
use config::appender::Appender;
use config::appender_ref::AppenderRef;
use config::Config;
use config::logger::Logger as ConfigLogger;
use log::{Log, LogLevel, LogLevelFilter, LogMetadata, LogRecord};
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::iter::Iterator;
use std::sync::{Arc, Mutex};

#[derive(Debug)]
struct GraphError {
    desc: String,
    detail: String,
}

impl fmt::Display for GraphError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: {}", self.desc, self.detail)
    }
}

impl Error for GraphError {
    fn description(&self) -> &str {
        &self.desc[..]
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

#[derive(Debug)]
struct LoggerGraph {
    nodes: Vec<LoggerNode>,
    edges: Vec<LoggerEdge>,
}

impl LoggerGraph {
    fn new() -> LoggerGraph {
        LoggerGraph {
            nodes: Vec::new(),
            edges: Vec::new(),
        }
    }

    fn add_node(&mut self, node: LoggerNode) -> usize {
        let idx = self.nodes.len();

        for (i, cnode) in self.nodes.iter().enumerate() {
            if cnode.name == node.name {
                return i;
            }
        }

        self.nodes.push(node);
        idx
    }

    fn add_edge(&mut self, source: usize, target: usize) -> Result<(), GraphError> {
        if source == 0 {
            return Err(GraphError {
                desc: "AddEdgeError".to_owned(),
                detail: "Unable to add outgoing edge from root".to_owned(),
            });
        } else {
            let eidx = self.edges.len();
            let node = &mut self.nodes[source];
            self.edges.push(LoggerEdge {
                target_node: target,
                next_edge: node.parent_edge,
            });
            node.parent_edge = Some(eidx);
        }

        Ok(())
    }

    // fn max_log_level(&self) -> LogLevelFilter {
    //     let mut max = LogLevelFilter::Off;
    //
    //     for node in &self.nodes {
    //         if let Some(ref l) = node.logger {
    //             max = cmp::max(max, l.level);
    //         }
    //     }
    //
    //     max
    // }

    fn find(&self, target: &str) -> usize {
        let rns: Vec<&str> = target.rsplit("::").collect();
        let mut res = 0;

        'outer: for ns in rns {
            for (i, node) in self.nodes.iter().enumerate() {
                if ns == node.name {
                    let ptr = self.path_to_root(i);
                    let mut nsv = Vec::new();
                    for idx in ptr.iter().rev() {
                        if let Some(n) = self.nodes.get(*idx) {
                            if !(&n.name[..] == "root") {
                                nsv.push(&n.name[..])
                            }
                        }
                    }

                    let test_target = nsv.join("::");
                    let mut starts_with = String::from("^");
                    starts_with.extend(test_target.chars());
                    match Regex::new(&starts_with[..]) {
                        Ok(r) => {
                            if r.is_match(target) {
                                res = i;
                                break 'outer;
                            }
                        }
                        Err(_) => {
                            if target == test_target {
                                res = i;
                                break 'outer;
                            }
                        }
                    }

                }
            }
        }

        res
    }

    fn path_to_root(&self, source: usize) -> Vec<usize> {
        let mut res = Vec::new();
        res.push(source);
        let mut parent_edge = self.nodes[source].parent_edge;
        loop {
            match parent_edge {
                None => break,
                Some(pe) => {
                    let target_node = self.edges[pe].target_node;
                    res.push(target_node);
                    parent_edge = self.nodes[target_node].parent_edge;
                }
            }
        }
        res
    }

    fn log(&mut self,
           record: &LogRecord,
           source: usize,
           ignore_level: bool,
           appenders: &mut Vec<Appender>) {
        let path = self.path_to_root(source);

        'log: for idx in path {
            if let Some(ref n) = self.nodes.get(idx) {
                if let Some(ref l) = n.logger {
                    if ignore_level || l.enabled(record.level()) {
                        if !l.check_filters(record) {
                            break 'log;
                        }

                        for (&idx, filters) in &l.ar {
                            for f in filters {
                                match f.filter(record) {
                                    Accept => break,
                                    Neutral => continue,
                                    Deny => break 'log,
                                }
                            }

                            if let Err(err) = appenders[idx].append(record) {
                                let _ = ::handle_error(&*err);
                            }
                        }
                    }
                    if !l.additivity {
                        break 'log;
                    }
                }
            }
        }
    }
}

#[derive(Debug)]
struct LoggerNode {
    name: String,
    logger: Option<TsurLogger>,
    parent_edge: Option<usize>,
}

#[derive(Debug)]
struct LoggerEdge {
    target_node: usize,
    next_edge: Option<usize>,
}

struct TsurLogger {
    /// Log level filter.
    level: LogLevelFilter,
    /// Indices into the appender vector, with mapping to optional appender ref filters.
    ar: HashMap<usize, Vec<Box<Filter>>>,
    /// Log level filters
    filters: Vec<Box<Filter>>,
    /// Additivity.  If false, the log event won't be passed to predecessors.
    additivity: bool,
}

impl fmt::Debug for TsurLogger {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("TsurLogger")
         .field("level", &self.level)
         .field("ar", &self.ar.keys().collect::<Vec<_>>())
         .field("filters", &self.filters.len())
         .field("additivity", &self.additivity)
         .finish()
    }
}

impl TsurLogger {
    fn check_filters(&self, record: &LogRecord) -> bool {
        let mut ret = true;

        for filter in &self.filters {
            match filter.filter(record) {
                Accept => break,
                Neutral => continue,
                Deny => {
                    ret = false;
                    break;
                }
            }
        }

        ret
    }

    fn enabled(&self, level: LogLevel) -> bool {
        self.level >= level
    }
}

pub struct InnerLogger {
    graph: LoggerGraph,
    filters: Vec<Box<Filter>>,
    appenders: Vec<Appender>,
}

fn appender_idxs(logger: &ConfigLogger,
                 appenders: &[Appender])
                 -> HashMap<usize, Vec<Box<Filter>>> {
    let names = appenders.iter()
                         .enumerate()
                         .map(|(i, appender)| (appender.name(), i))
                         .collect::<HashMap<_, _>>();

    match logger.appender_ref() {
        Some(ref ar) => {
            ar.iter()
              .map(|a| {
                  let b: AppenderRef = a.into();
                  (names[&b.name], b.filters)
              })
              .collect::<HashMap<_, _>>()
        }
        None => HashMap::new(),
    }
}

impl InnerLogger {
    pub fn new(config: Config) -> InnerLogger {
        let (root, loggers, appenders, filters) = config.unpack();
        assert!(root.len() == 1);

        let mut graph = LoggerGraph::new();
        let rtl = TsurLogger {
            level: match root[0].level() {
                Some(l) => l,
                None => LogLevelFilter::Off,
            },
            additivity: match root[0].additive() {
                Some(a) => a,
                None => true,
            },
            filters: match root[0].filter() {
                Some(ref f) => f.clone().into(),
                None => Vec::new(),
            },
            ar: appender_idxs(&root[0], &appenders),
        };

        let root_idx = graph.add_node(LoggerNode {
            name: root[0].name(),
            logger: Some(rtl),
            parent_edge: None,
        });

        for logger in &loggers {
            let mut logger_nodes = logger.name()
                                         .rsplit("::")
                                         .map(|n| {
                                             LoggerNode {
                                                 name: n.to_owned(),
                                                 logger: None,
                                                 parent_edge: None,
                                             }
                                         })
                                         .collect::<Vec<LoggerNode>>();

            let tl = TsurLogger {
                level: match logger.level() {
                    Some(l) => l,
                    None => LogLevelFilter::Off,
                },
                additivity: match logger.additive() {
                    Some(a) => a,
                    None => true,
                },
                filters: match logger.filter() {
                    Some(ref f) => f.clone().into(),
                    None => Vec::new(),
                },
                ar: appender_idxs(logger, &appenders),
            };

            logger_nodes[0].logger = Some(tl);

            let mut target_idx = root_idx;
            for node in logger_nodes.into_iter().rev() {
                let source_idx = graph.add_node(node);
                let _ = graph.add_edge(source_idx, target_idx);
                target_idx = source_idx;
            }
        }

        InnerLogger {
            graph: graph,
            appenders: appenders,
            filters: filters,
        }
    }
}

pub struct Logger {
    inner: Arc<Mutex<InnerLogger>>,
}

impl Logger {
    pub fn new(config: Config) -> Logger {
        Logger { inner: Arc::new(Mutex::new(InnerLogger::new(config))) }
    }

    // pub fn max_log_level(&self) -> LogLevelFilter {
    //     match self.inner.lock() {
    //         Ok(lck) => lck.graph.max_log_level(),
    //         Err(e) => {
    //             println!("Error acquiring mutex lock! {:?}", e);
    //             LogLevelFilter::Trace
    //         }
    //     }
    // }

    pub fn inner(&self) -> Arc<Mutex<InnerLogger>> {
        self.inner.clone()
    }
}

impl Log for Logger {
    fn enabled(&self, metadata: &LogMetadata) -> bool {
        match self.inner.lock() {
            Ok(mut lck) => {
                let inner = &mut *lck;
                // Context-wide Filters are configured directly in the configuration. Events that
                // are rejected by these filters will not be passed to loggers for further
                // processing. Once an event has been accepted by a Context-wide filter it will not
                // be evaluated by any other Context-wide Filters nor will the Logger's Level be
                // used to filter the event. The event will be evaluated by Logger and Appender
                // Filters however.
                let mut res = Vec::new();
                for filter in &mut inner.filters {
                    match filter.filter_by_meta(metadata) {
                        Accept => {
                            res.push(true);
                            break;
                        }
                        Neutral => res.push(true),
                        Deny => {
                            res.push(false);
                            break;
                        }
                    }
                }
                res.iter().all(|x| *x)
            }
            Err(e) => {
                println!("Error acquiring mutex lock! {:?}", e);
                false
            }
        }
    }

    fn log(&self, record: &LogRecord) {
        match self.inner.lock() {
            Ok(mut lck) => {
                let inner = &mut *lck;
                // Context-wide Filters are configured directly in the configuration. Events that
                // are rejected by these filters will not be passed to loggers for further
                // processing. Once an event has been accepted by a Context-wide filter it will not
                // be evaluated by any other Context-wide Filters nor will the Logger's Level be
                // used to filter the event. The event will be evaluated by Logger and Appender
                // Filters however.
                let mut ignore_level = false;
                for filter in &mut inner.filters {
                    match filter.filter(record) {
                        Accept => {
                            ignore_level = true;
                            break;
                        }
                        Neutral => continue,
                        Deny => return,
                    }
                }

                let sidx = inner.graph.find(record.target());
                inner.graph.log(record, sidx, ignore_level, &mut inner.appenders);
            }
            Err(e) => {
                println!("Error acquiring mutex lock! {:?}", e);
            }
        }
    }
}

#[cfg(test)]
mod test {
    use config::Config;
    use config::test::TEST_CONFIG;
    use super::InnerLogger;

    #[test]
    fn test_inner_logger() {
        match Config::new(TEST_CONFIG) {
            Ok(c) => {
                let graph = InnerLogger::new(c).graph;

                // assert!(graph.max_log_level() == LogLevelFilter::Trace);
                assert!(graph.find("config::filter::burst") == 3);
                assert!(graph.nodes.len() == 6);

                // root
                assert!(graph.path_to_root(0) == vec![0]);
                // config::root
                assert!(graph.path_to_root(1) == vec![1, 0]);
                // filter::config::root
                assert!(graph.path_to_root(2) == vec![2, 1, 0]);
                // burst::filter::config::root
                assert!(graph.path_to_root(3) == vec![3, 2, 1, 0]);
                // appender::config::root
                assert!(graph.path_to_root(4) == vec![4, 1, 0]);
                // file::appender::config::root
                assert!(graph.path_to_root(5) == vec![5, 4, 1, 0]);
            }
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn test_default() {
        match Config::new("") {
            Ok(c) => {
                let il = InnerLogger::new(c);
                // assert!(il.graph.max_log_level() == LogLevelFilter::Error);
                assert!(il.graph.nodes.len() == 1);
                assert!(il.graph.edges.is_empty());
                assert!(il.appenders.len() == 1);
                assert!(il.filters.is_empty());
            }
            Err(_) => assert!(false),
        }
    }
}
