# vergen
## Version
[![Crates.io](https://img.shields.io/crates/v/tsur4gol.svg)](https://crates.io/crates/tsur4gol)
[![Build
Status](https://travis-ci.org/rustyhorde/tsur4gol.svg?branch=master)](https://travis-ci.org/rustyhorde/tsur4gol)
[![Coverage Status](https://coveralls.io/repos/github/rustyhorde/tsur4gol/badge.svg?branch=master)](https://coveralls.io/github/rustyhorde/tsur4gol?branch=master)

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
